@extends('layouts.user.base')
@section('content')


<!-- <style type="text/css">
    #carousel-1 img {
      height: auto;
    }

    /* Change the order of the indicators. 
       Return them to the center of the slide. */
    .invitees_carousel_indiators {
      width: auto;
      margin-left: 0;
      transform: translateX(-50%);
    }
    .invitees_carousel_indiators li {
      float: right;
      margin: 1px 4px;
    }
    .invitees_carousel_indiators .active {
      margin: 0 3px;
    }

    /* Change the direction of the transition. */
    @media all and (transform-3d), (-webkit-transform-3d) {
      .carousel-inner > .carousel-item.carousel-control-next,
      .carousel-inner > .carousel-item.active.right {
        left: 0;
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0, 0);
      }
      .carousel-inner > .carousel-item.carousel-control-prev,
      .carousel-inner > .carousel-item.active.left {
        left: 0;
        -webkit-transform: translate3d(100%, 0, 0);
        transform: translate3d(100%, 0, 0);
      }
    }
</style> -->

<!-- Carousel Banners -->

<section class="container-fluid my-0">
    <div class="row">
        <div class="col-12 pt2 pb-0 px-0">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100 my-auto" src="images/NEW TTA Mega Convention Flyer.jpg" alt="First slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 my-auto" src="images/NEW TTA Mega Convention Flyer.jpg" alt="Second slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 my-auto" src="images/NEW TTA Mega Convention Flyer.jpg" alt="Third slide" />
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- End of Carousel Banners -->

<!-- Invitees -->

<!-- This is For Desktop Version -->
<div class="container-fluid px-0">
    <div class="mx-auto main-heading w-100 mb-0">
        <span class="convention_invitees_heading">Convention Invitees</span>
    </div>
</div>
<div class="container-fluid invitees_bg">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-lg-2 d-none d-lg-block">
                    <div>
                        <img src="images/welcome-image.png" alt="" border="0" class="img-fluid left-welcome-image" width="90" />
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-8 px-md-5 px-lg-3 pt-sm-40 py-md-5 pb-3">
                    <marquee behavior="alternate" class="px-5">
                        <div class="row flex-wrap-nowrap">
                            <div>
                                <div class="invitees d-flex">
                                    <div class="p-2">
                                        <img src="images/suma.kanakala.jpg" class="img-fluid mx-auto d-block border-radius-10" width="120px" alt="" />
                                    </div>
                                    <div class="my-auto">
                                        <h6 class="text-purple font-weight-bold">Suma Kanakala</h6>
                                        <div class="text-purple">Anchor</div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="invitees d-flex">
                                    <div class="p-2">
                                        <img src="images/sp.sailaja.jpg" class="img-fluid mx-auto d-block border-radius-10" width="120px" alt="" />
                                    </div>
                                    <div class="my-auto">
                                        <h6 class="text-purple font-weight-bold">S.P. Sailaja</h6>
                                        <div class="text-purple">Singer</div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="invitees d-flex">
                                    <div class="p-2">
                                        <img src="images/sp.charan.jpg" class="img-fluid mx-auto d-block border-radius-10" width="120px" alt="" />
                                    </div>
                                    <div class="my-auto">
                                        <h6 class="text-purple font-weight-bold">S.P. Charan</h6>
                                        <div class="text-purple">Singer</div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div class="invitees d-flex">
                                    <div class="p-2">
                                        <img src="images/sunitha.jpg" class="img-fluid mx-auto d-block border-radius-10" width="120px" alt="" />
                                    </div>
                                    <div class="my-auto">
                                        <h6 class="text-purple font-weight-bold">Sunitha</h6>
                                        <div class="text-purple">Singer</div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="invitees d-flex">
                                    <div class="p-2">
                                        <img src="images/usha-singer.jpg" class="img-fluid mx-auto d-block border-radius-10" width="120px" alt="" />
                                    </div>
                                    <div class="my-auto">
                                        <h6 class="text-purple font-weight-bold">Usha</h6>
                                        <div class="text-purple">Singer</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </marquee>
                </div>
                <div class="col-lg-2 d-none d-lg-block">
                    <div>
                        <img src="images/welcome-image.png" alt="" border="0" class="img-fluid right-welcome-image" width="90" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- This is For Mobile Version -->

<!-- <div class="container-fluid invitees_bg d-block d-md-none">
    <div class="row">
        <div class="col-12">
            <div class="horizontal-scroll">
                <div class="invitee-height">
                    <div class="invitee-width">
                        <div class="invitees d-flex">
                            <div class="p-2">
                                <img src="images/suma.kanakala.jpg" class="img-fluid mx-auto d-block border-radius-10" width="100px" alt="" />
                            </div>
                            <div class="my-auto">
                                <h6 class="text-purple font-weight-bold">Suma Kanakala</h6>
                                <div class="text-purple">Anchor</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="invitee-height">
                    <div class="invitee-width">
                        <div class="invitees d-flex">
                            <div class="p-2">
                                <img src="images/sp.sailaja.jpg" class="img-fluid mx-auto d-block border-radius-10" width="100px" alt="" />
                            </div>
                            <div class="my-auto">
                                <h6 class="text-purple font-weight-bold">S.P. Sailaja</h6>
                                <div class="text-purple">Singer</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="invitee-height">
                    <div class="invitee-width">
                        <div class="invitees d-flex">
                            <div class="p-2">
                                <img src="images/sp.charan.jpg" class="img-fluid mx-auto d-block border-radius-10" width="100px" alt="" />
                            </div>
                            <div class="my-auto">
                                <h6 class="text-purple font-weight-bold">S.P. Charan</h6>
                                <div class="text-purple">Singer</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="invitee-height">
                    <div class="invitee-width">
                        <div class="invitees d-flex">
                            <div class="p-2">
                                <img src="images/singer-sunitha.jpg" class="img-fluid mx-auto d-block border-radius-10" width="100px" alt="" />
                            </div>
                            <div class="my-auto">
                                <h6 class="text-purple font-weight-bold">Sunitha</h6>
                                <div class="text-purple">Singer</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="invitee-height">
                    <div class="invitee-width">
                        <div class="invitees d-flex">
                            <div class="p-2">
                                <img src="images/singer-usha.jpg" class="img-fluid mx-auto d-block border-radius-10" width="100px" alt="" />
                            </div>
                            <div class="my-auto">
                                <h6 class="text-purple font-weight-bold">Usha</h6>
                                <div class="text-purple">Singer</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 -->
<!-- End of Invitees -->

<!-- Event Schedule -->

<section class="container-fluid" style="margin-top: -16px;">
    <div class="row px-1">
        <div class="col-12 col-md-12 col-lg-3 my-1 my-lg-auto px-2 schedule-date-before-triangle">
            <a href="calender.php" class="text-decoration-none">
                <div class="schedule-date-bg">
                    <div class="schedule-date-bg-dots-img">
                        <div class="icon1">
                            <div class="py-5 py-lg-75 text-center">
                                <span class="fs25 text-white">May 27<sup>th</sup> to 29<sup>th</sup>, 2022<br /></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-12 col-md-12 col-lg-6 my-1 my-lg-auto px-2 location-before-triangle">
            <div class="schedule-location-bg">
                <div class="location-bg-dots-img">
                    <div class="icon2">
                        <div class="py-5 py-lg-63point5 px-1 px-sm-0">
                            <h4 class="text-white text-left text-center fs23">New Jersey Convention And Exposition Center</h4>
                            <div class="text-left text-white text-center fs16">97 Sunfield Ave, Edison, NJ 08837, United States</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-3 my-1 my-lg-auto px-2 schedule-register-before-triangle">
            <div class="schedule-register-bg">
                <div class="schedule-date-bg-dots-img">
                    <div class="text-center text-lg-right text-xl-center py-5 py-lg-70">
                       <a href="{{url('bookticket')}}" class="btn register-today-btn">REGISTER TODAY</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Event Schedule -->

<!-- Convention Messages -->

<section class="container-fluid pt20 pb80">
    <div class="row justify-content-center">
        <div class="col-12 col-md-12 col-lg-12 py-3">
            <div class="row">
                @foreach($messages as $message)
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                    <h6 class="president-message mb-3 text-center">{{$message->designation->name}} Message</h6>
                    <div class="shadow-small border-radius-10 president-msg_bg message-bg-{{ $loop->iteration }}">
                        <div class="msg_flower_bg d-flex">
                            <div class="p-3 my-auto">
                                <img src="{{asset(config('conventions.message_display').$message->image_url)}}" class="img-fluid rounded-circle" width="110px" height="110px" alt="Mohan Patalolla" style="border: 3px solid #f9a565;" />
                            </div>
                            <div class="pt-0 pb-2 text-white my-auto">
                                <div class="text-center mb-1 heading-font fs18 font-weight-bold">{{$message->name}}</div>
                                <div class="text-center text-white fs14">{{$message->designation->name}}</div>
                            </div>
                        </div>
                        <div class="text-white px-3 pb-4">
                            <p class="pb-2 mb-0 text-center description">
                                 {!! substr($message->message, 0, 100) !!}....
                            </p>
                            <div class="text-center pt-3">
                                <a href="{{url('message_content',$message->id)}}">
                                    <i class="fas fa-chevron-circle-right text-white fs20"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                
            </div>
        </div>
    </div>
</section>

<!-- End of Convention Messages -->

<!-- Convention Team -->


    <section class="container-fluid pb80 convention-team-bg">
        <div class="mx-auto main-heading">
            <span>Convention Team</span>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div>
                        <ul class="list-unstyled text-center horizontal-scroll">
                            <li class="convention-team-nav-item d-inline-flex">
                                <a class="convention-team-nav-link text-halfwhite fs18 pt10 pb10 text-decoration-none convention-team_active_tab_btn" target-id="#convention_advisory_committee">Convention&nbsp;Advisory&nbsp;Committee</a>
                            </li>
                            <li class="convention-team-nav-item d-inline-flex">
                                <a class="convention-team-nav-link text-halfwhite fs18 pt10 pb10 text-decoration-none" target-id="#convention_executive_committee">Convention&nbsp;Executive&nbsp;Committee</a>
                            </li>
                            <li class="convention-team-nav-item d-inline-flex">
                                <a class="convention-team-nav-link text-halfwhite fs18 pt10 pb10 text-decoration-none" target-id="#convention_regional_advisors">Convention&nbsp;Regional&nbsp;Advisors</a>
                            </li>
                        </ul>
                        <!-- 
                        <ul class="list-unstyled text-center horizontal-scroll">
                            <li class="convention-team-nav-item d-inline-flex">
                                <a class="convention-team-nav-link text-dark fs18 pt10 pb10 text-decoration-none convention-team_active_tab_btn" target-id="#convention_advisory_committee">Convention&nbsp;Advisory&nbsp;Committee</a>
                            </li>
                            <li class="convention-team-nav-item d-inline-flex">
                                <a class="convention-team-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#convention_executive_committee">Convention&nbsp;Executive&nbsp;Committee</a>
                            </li>
                            <li class="convention-team-nav-item d-inline-flex">
                                <a class="convention-team-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#convention_regional_advisors">Convention&nbsp;Regional&nbsp;Advisors</a>
                            </li>
                        </ul> -->
                    </div>
                </div>
            </div>
            <div class="row justify-content-center convention-team_active_tab" id="convention_advisory_committee">
                <div class="col-12 col-md-10 col-lg-10">
                    <div class="row py-3">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/mallareddy.png" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Pailla Malla Reddy" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Dr. Pailla Malla Reddy</h6>
                                    <div class="text-center pb-2 text-halfwhite">Founder</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/vijay.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Vijayapal Reddy" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Dr. Vijayapal Reddy</h6>
                                    <div class="text-center pb-2 text-halfwhite">Chair</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/haranath_policherla.png" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Haranath Policherla" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Dr. Haranath Policherla</h6>
                                    <div class="text-center pb-2 text-halfwhite">Co-Chair</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/Mohan-Patalolla_1.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Mohan Reddy Patalolla" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Dr. Mohan Reddy Patalolla</h6>
                                    <div class="text-center pb-2 text-halfwhite">Council Chair</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center" id="convention_executive_committee" style="display: none;">
                <div class="col-12 col-md-10 col-lg-10 py-3">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/Mohan-Patalolla_1.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Dr.Mohan Patalolla" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Mohan Reddy Patalolla</h6>
                                    <!-- <div class="text-center pb-2 text-halfwhite">President</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/Srinivas_Ganagoni.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Dr.Mohan Patalolla" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Srinivas Ganagoni</h6>
                                    <!-- <div class="text-center pb-2 text-halfwhite">Convener</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/vamshi_Reddy_2.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Vamshi Reddy" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Vamshi Reddy</h6>
                                    <!-- <div class="text-center pb-2 text-halfwhite">President-Elect</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/Bharath_Madadi_12.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Bharath Madadi" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Bharath Madadi</h6>
                                    <!-- <div class="text-center pb-2 text-halfwhite">Past President</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/Suresh_venkannagari_3.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Suresh Reddy Venkannagari" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Suresh Reddy Venkannagari</h6>
                                    <!-- <div class="text-center pb-2 text-halfwhite">Executive Vice-President</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/Srini-M_4.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Srinivasa Manapragada" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Srinivasa Manapragada</h6>
                                    <!-- <div class="text-center pb-2 text-halfwhite">Genaral Secretary</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/Pavan Ravva_6.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Pavan Ravva" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Pavan Ravva</h6>
                                    <!-- <div class="text-center pb-2 text-halfwhite">Treasurer</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/venkat-gaddam_5.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Venkat Gaddam" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Venkat Gaddam</h6>
                                    <!-- <div class="text-center pb-2 text-halfwhite">Executive Director</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/kvmadam.jpeg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Kavitha Reddy" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Kavitha Reddy</h6>
                                    <!-- <div class="text-center pb-2 text-halfwhite">Joint Secretary</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/Naveen_Goli_10.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" width="150" alt="" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Naveen Goli</h6>
                                    <!-- <div class="text-center pb-2 text-halfwhite">International Vice-President</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/venkat_aekka_9.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Venkat Aekka" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Venkat Aekka</h6>
                                    <!-- <div class="text-center pb-2 text-halfwhite">National Co-Ordinator</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/harinder-tallapalli_8.png" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Harinder Tallapalli" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Harinder Tallapally</h6>
                                   <!--  <div class="text-center pb-2 text-halfwhite">Joint Treasurer</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/narasimha_reddy_ln.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Narasimha Reddy Donthireddy" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Narasimha Reddy Donthireddy(LN)</h6>
                                    <!-- <div class="text-center pb-2 text-halfwhite">Media & Communication Director</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/sole_madhavi_11.png" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Madhavi Soleti" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Madhavi Soleti</h6>
                                    <!-- <div class="text-center pb-2 text-halfwhite">Ehtics Committee Co-Ordinator</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/Gangadhar_Vuppala.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="user" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Gangadhar Vuppala</h6>
                                    <div class="text-center pb-2 text-halfwhite"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="convention-team">
                                <div>
                                    <img src="images/Usha-Mannem.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Usha Mannam" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Usha Mannam</h6>
                                    <div class="text-center pb-2 text-halfwhite"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center" id="convention_regional_advisors" style="display: none;">
                <div class="col-12 col-md-10 col-lg-10 py-3">
                    <div class="row">
                        <div class="col-12 col-lg-6 offset-lg-3">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-4 col-lg-6">
                                    <div class="convention-team">
                                        <div>
                                            <img src="images/malepic.jpeg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Santhosh Pathuri" />
                                        </div>
                                        <div class="pt-3">
                                            <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Santhosh Pathuri, NJ</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4 col-lg-6">
                                    <div class="convention-team">
                                        <div>
                                            <img src="images/malepic.jpeg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Manohar Kasagani" />
                                        </div>
                                        <div class="pt-3">
                                            <h6 class="text-halfwhite text-center mb-1 font-weight-bold">Manohar Kasagani, Dallas</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End of Convention Team -->

    <!-- Standing Committee -->

<div class="side-border-bg">
    <section class="container-fluid pb80">
        <div class="mx-auto main-heading">
            <div>
                <span>Convention committees</span>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 my-5 pt-4 pb-0">
                    <h4 class="text-center text-dark">Coming Soon</h4>
                </div>
            </div>  
        </div>
        <!-- <div class="container">
            <div class="row">
                <div class="w-100">
                    <div class="scroller scroller-left float-left mt-2"><i class="fa fa-chevron-left text-red"></i></div>
                    <div class="scroller scroller-right float-right mt-2"><i class="fa fa-chevron-right text-red"></i></div>
                    <div class="wrapper">
                        <nav class="nav nav-tabs list mt-2 border-bottom-0" id="myTab" role="tablist">
                            <a class="nav-link nav-item text-dark active" href="#banquette" role="tab" data-toggle="tab">Banquette</a>
                            <a class="nav-link nav-item text-dark" href="#media_n_communication" role="tab" data-toggle="tab">Media&nbsp;&&nbsp;Communication</a>
                            <a class="nav-link nav-item text-dark" href="#spiritual" role="tab" data-toggle="tab">Spiritual</a>
                            <a class="nav-link nav-item text-dark" href="#budget_n_finance" role="tab" data-toggle="tab">Budget&nbsp;&&nbsp;Finance</a>
                            <a class="nav-link nav-item text-dark" href="#overseas_coordination" role="tab" data-toggle="tab">Overseas&nbsp;Coordination</a>
                            <a class="nav-link nav-item text-dark" href="#stage_n_av" role="tab" data-toggle="tab">Stage&nbsp;&&nbsp;AV</a>
                            <a class="nav-link nav-item text-dark" href="#business" role="tab" data-toggle="tab">Business</a>
                            <a class="nav-link nav-item text-dark" href="#political_forum" role="tab" data-toggle="tab">Political&nbsp;Forum</a>
                            <a class="nav-link nav-item text-dark" href="#tta_star" role="tab" data-toggle="tab">TTA&nbsp;Star</a>
                            <a class="nav-link nav-item text-dark" href="#cme" role="tab" data-toggle="tab">CME</a>
                            <a class="nav-link nav-item text-dark" href="#programs_n_events" role="tab" data-toggle="tab">Programs&nbsp;and&nbsp;Events</a>
                            <a class="nav-link nav-item text-dark" href="#transportation" role="tab" data-toggle="tab">Transportation</a>
                            <a class="nav-link nav-item text-dark" href="#cultural" role="tab" data-toggle="tab">Cultural</a>
                            <a class="nav-link nav-item text-dark" href="#reception" role="tab" data-toggle="tab">Reception</a>
                            <a class="nav-link nav-item text-dark" href="#vendors_n_exhibits" role="tab" data-toggle="tab">Vendors&nbsp;&&nbsp;Exhibits</a>
                            <a class="nav-link nav-item text-dark" href="#decoration" role="tab" data-toggle="tab">Decoration</a>
                            <a class="nav-link nav-item text-dark" href="#registration" role="tab" data-toggle="tab">Registration</a>
                            <a class="nav-link nav-item text-dark" href="#web_committee" role="tab" data-toggle="tab">Web&nbsp;committee</a>
                            <a class="nav-link nav-item text-dark" href="#food" role="tab" data-toggle="tab">Food</a>
                            <a class="nav-link nav-item text-dark" href="#safety_n_security" role="tab" data-toggle="tab">Safety&nbsp;&&nbsp;Security</a>
                            <a class="nav-link nav-item text-dark" href="#women_committee" role="tab" data-toggle="tab">Women&nbsp;committee</a>
                            <a class="nav-link nav-item text-dark" href="#hospitality" role="tab" data-toggle="tab">Hospitality</a>
                            <a class="nav-link nav-item text-dark" href="#short_film" role="tab" data-toggle="tab">Short&nbsp;Film</a>
                            <a class="nav-link nav-item text-dark" href="#youth_forum" role="tab" data-toggle="tab">Youth&nbsp;Forum</a>
                            <a class="nav-link nav-item text-dark" href="#literacy" role="tab" data-toggle="tab">Literacy</a>
                            <a class="nav-link nav-item text-dark" href="#social_media" role="tab" data-toggle="tab">Social&nbsp;Media</a>
                            <a class="nav-link nav-item text-dark" href="#matrimonial" role="tab" data-toggle="tab">Matrimonial</a>
                            <a class="nav-link nav-item text-dark" href="#souvenir" role="tab" data-toggle="tab">Souvenir</a>
                        </nav>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="tab-content p-3" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="banquette">
                        <div class="col-12 col-md-10 offset-md-1 col-lg-10 offset-lg-1 py-3">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                                    <div class="convention-committee-member-block">
                                        <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block" alt="Malla Reddy Karra" />
                                        <div class="text-center pt-3">
                                            <h6 class="text-violet mb-1">Malla Reddy Karra</h6>
                                            <div>Audit Committee - Chair</div>
                                            <div>Boston</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                                    <div class="convention-committee-member-block">
                                        <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block" alt="Vishnu Maddu" />
                                        <div class="text-center pt-3">
                                            <h6 class="text-violet mb-1">Vishnu Maddu</h6>
                                            <div>Audit Committee - Co-Chair</div>
                                            <div>Atlanta</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                                    <div class="convention-committee-member-block">
                                        <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block" alt="Malla Reddy Karra" />
                                        <div class="text-center pt-3">
                                            <h6 class="text-violet mb-1">Vijaypal Reddy</h6>
                                            <div>Bylaws Committe - Chair</div>
                                            <div>Atlanta</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                                    <div class="convention-committee-member-block">
                                        <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block" alt="Mohan Patalolla" />
                                        <div class="text-center pt-3">
                                            <h6 class="text-violet mb-1">Mohan Patalolla</h6>
                                            <div>Bylaws Committee - Co-Chair</div>
                                            <div>New Jersey</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                                    <div class="convention-committee-member-block">
                                        <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block" alt="Suresh Reddy Kokatam" />
                                        <div class="text-center pt-3">
                                            <h6 class="text-violet mb-1">Suresh Reddy Kokatam</h6>
                                            <div>Ethics Committee - Chair</div>
                                            <div>TBU</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                                    <div class="convention-committee-member-block">
                                        <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block" alt="Venkat Nalluri Kokatam" />
                                        <div class="text-center pt-3">
                                            <h6 class="text-violet mb-1">Venkat Nalluri</h6>
                                            <div>Ethics Committee - Co-Chair</div>
                                            <div>TBU</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                                    <div class="convention-committee-member-block">
                                        <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block" alt="Bhasker Pinna" />
                                        <div class="text-center pt-3">
                                            <h6 class="text-violet mb-1">Bhasker Pinna</h6>
                                            <div>Website Committee - Chair</div>
                                            <div>Pennsylvania</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                                    <div class="convention-committee-member-block">
                                        <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block" alt="Nidheesh Raju Baskaruni" />
                                        <div class="text-center pt-3">
                                            <h6 class="text-violet mb-1">Nidheesh Raju Baskaruni</h6>
                                            <div>Website Committee - Co-Chair</div>
                                            <div>Atlanta</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                                    <div class="convention-committee-member-block">
                                        <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block" alt="Venugopal Darur" />
                                        <div class="text-center pt-3">
                                            <h6 class="text-violet mb-1">Venugopal Darur</h6>
                                            <div>Website Committee - Member</div>
                                            <div>TBU</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                                    <div class="convention-committee-member-block">
                                        <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block" alt="Ashok Chintakunta" />
                                        <div class="text-center pt-3">
                                            <h6 class="text-violet mb-1">Ashok Chintakunta</h6>
                                            <div>Cultural Committee - Chair</div>
                                            <div>New York</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                                    <div class="convention-committee-member-block">
                                        <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block" alt="Suman Mudamba" />
                                        <div class="text-center pt-3">
                                            <h6 class="text-violet mb-1">Suman Mudamba</h6>
                                            <div>Cultural Committee - Co-Chair</div>
                                            <div>Delaware</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                                    <div class="convention-committee-member-block">
                                        <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block" alt="" />
                                        <div class="text-center pt-3">
                                            <h6 class="text-violet mb-1">Deepthi Reddy</h6>
                                            <div>Cultural Committee - Co-Chair</div>
                                            <div>Atlanta</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 pt-3">
                                    <div class="text-right">
                                        <a href="convention_committee_details.php" class="text-decoration-none events-more-details-btn border-radius-20 fs16 btn-lg">View More <i class="fas fa-angle-right pl-2 see_more_arrow"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </section>

    <!-- End of Standing Committee -->

    <!-- Donors -->

    <section class="container-fluid pb80 donors-bg">
        <div class="mx-auto main-heading">
            <span>Convention Donors</span>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 my-5 pt-4 pb-5">
                    <h4 class="text-center text-white">Coming Soon</h4>
                </div>
            </div>  
            <!-- <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-10 py-3">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 py-3 my-1">
                            <div class="convention-donor">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Dr. Harnath Policherla</h6>
                                    <div class="text-center pb-2">Michigan</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 py-3 my-1">
                            <div class="convention-donor">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Dr.Mohan Patalolla</h6>
                                    <div class="text-center pb-2">New Jersey</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 py-3 my-1">
                            <div class="convention-donor">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Malla Reddy Karra</h6>
                                    <div class="text-center pb-2">Boston</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 py-3 my-1">
                            <div class="convention-donor">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Vishnu Maddu</h6>
                                    <div class="text-center pb-2">Atlanta</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </section>

    <!-- End of Donors -->

    <!-- Events -->

    <section class="container-fluid pb80">
        <div class="mx-auto main-heading">
            <span>Convention Events Schedule</span>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 my-5 pt-5 pb-4">
                    <h4 class="text-center">Coming Soon</h4>
                </div>
            </div>
            <!-- <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-10 py-3">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Breakfast-and-matrimony.jpg" class="img-fluid border-radius-15 w-100" />
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Breakfast and Matrimony</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class=""><i class="far fa-calendar-alt pr-2"></i>27 May 2022</div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm fs-xss-10">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Business-and-womens_conference3.jpg" class="img-fluid border-radius-15 w-100" />
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Business conference, Women’s Con...</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class=""><i class="far fa-calendar-alt pr-2"></i>28 May 2022</div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm fs-xss-10">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Laxmi-narsimhaswamy-kalyanam2.jpg" class="img-fluid border-radius-15 w-100" />
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Sri Laxmi Narasimha Swamy Kalyanam</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class=""><i class="far fa-calendar-alt pr-2"></i>29 May 2022</div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm fs-xss-10">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Breakfast-and-matrimony.jpg" class="img-fluid border-radius-15 w-100" />
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Breakfast and Matrimony</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class=""><i class="far fa-calendar-alt pr-2"></i>27 May 2022</div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm fs-xss-10">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Business-and-womens_conference3.jpg" class="img-fluid border-radius-15 w-100" />
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Business conference, Women’s Con...</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class=""><i class="far fa-calendar-alt pr-2"></i>28 May 2022</div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm fs-xss-10">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Laxmi-narsimhaswamy-kalyanam2.jpg" class="img-fluid border-radius-15 w-100" />
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Sri Laxmi Narasimha Swamy Kalyanam</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class=""><i class="far fa-calendar-alt pr-2"></i>29 May 2022</div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm fs-xss-10">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </section>

    <!-- End of Events -->

    <!-- Media Partners -->

    <section class="container-fluid pb80">
        <div class="mx-auto main-heading">
            <span>Media partners</span>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-10 py-3">
                    <marquee behavior="alternate" class="px-5">
                        <div class="row flex-wrap-nowrap">
                            <div class="m-3">
                                <div class="meadia-partner-image">
                                    <img src="images/tv9.png" class="img-fluid mx-auto d-block" />
                                </div>
                            </div>
                            <div class="m-3">
                                <div class="meadia-partner-image">
                                    <img src="images/ntv.png" class="img-fluid mx-auto d-block" />
                                </div>
                            </div>
                            <div class="m-3">
                                <div class="meadia-partner-image">
                                    <img src="images/etv.png" class="img-fluid mx-auto d-block" />
                                </div>
                            </div>
                            <div class="my-3 ml-3 pr-5 mr-5">
                                <div class="meadia-partner-image">
                                    <img src="images/asia_tv.png" class="img-fluid mx-auto d-block" />
                                </div>
                            </div>
                        </div>
                    </marquee>  
                </div>
            </div>
        </div>
    </section>

    <!-- End of Media Partners -->
</div>
<!-- End Of Content -->



@section('javascript')
<script>
    $(".convention-team-nav-link").click(function () {
        //active_tab

        $(".convention-team_active_tab_btn").removeClass("convention-team-nav-link nav-item");
        $(".convention-team_active_tab_btn").removeClass("convention-team_active_tab_btn");
        $(this).addClass("convention-team-nav-link nav-item");
        $(this).addClass("convention-team_active_tab_btn");

        $(".convention-team_active_tab").hide();
        $(".convention-team_active_tab").removeClass("convention-team_active_tab");
        $($(this).attr("target-id")).addClass("convention-team_active_tab");
        $($(this).attr("target-id")).show();
    });
</script>

<script>
    $(".convention-committee-nav-link").click(function () {
        //active_tab
        $(".convention-committee_active_tab_btn").removeClass("convention-committee-nav-link");
        $(".convention-committee_active_tab_btn").removeClass("convention-committee_active_tab_btn");
        $(this).addClass("convention-committee-nav-link");
        $(this).addClass("convention-committee_active_tab_btn");

        $(".convention-committee_active_tab").hide();
        $(".convention-committee_active_tab").removeClass("convention-committee_active_tab");
        $($(this).attr("target-id")).addClass("convention-committee_active_tab");
        $($(this).attr("target-id")).show();
    });
</script>

@endsection

@endsection
