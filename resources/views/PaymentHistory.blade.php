@extends('layouts.user.base')
@section('content')



<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-30 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12">
                <h5>Transaction Details</h5>
                <a href="{{route('myaccount')}}" data-toggle="tooltip" title="" class="float-right-back-btn btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>

            </div>
            <div class="col-12">
                <div class="card-body px-0">
                    <div class="table-responsive">
                        @if(Auth::user()->registration_type_id!=0)
                        <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    <th>SNo</th>
                                    <th>Transaction Data</th>
                                    <th>Payment Methord</th>
                                    <th>Amount</th>
                                    <th>Payment Status</th>
                                    <th>Account Status</th>
                                    <th>Paid Towards</th>
                                    <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($payments as $payment)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        <b>Date</b>
                                    {{ $payment->created_at }}
                                    <br>
                                    <b>Transaction Id </b>
                                    {{ $payment->unique_id_for_payment }}
                                  
                                    </td>
                                    <td>{{ ucfirst($payment->paymentmethord->name)  }}</td>
                                    <td> $ {{ $payment->payment_amount }} </td>
                                    <td> {{ ucfirst($payment->payment_status) }} </td>
                                    <td> {{ ucfirst($payment->account_status) }} </td>
                                    <td> {{ ucfirst($payment->payment_made_towards) }} </td>
                                </tr>
                                @empty

                                @endforelse
                            </tbody>
                        </table>
                        @else
                        <h5> You have not Registered for the event Please click <a href="bookticket">here<a /> for register</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@section('javascript')

@endsection


@endsection
