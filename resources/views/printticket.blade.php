@extends('layouts.user.base')
@section('content')


<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-30 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12">
               <!--  <h5>Recipt</h5> -->
            </div>
            <div class="col-12">
                <div class="card-body px-0">
                    <div class="table-responsive">
                        <div id="print-div">
                            <h1 align="center"> NRIVA CONVENTIONS</h1>
                            <h6 align="center"> {{$venue->location}}</h6>
                            <h6 align="center"> Event dates {{ \Carbon\Carbon::parse($venue->Event_date_time)->isoFormat('MMM Do')}} To {{ \Carbon\Carbon::parse($venue->end_date)->isoFormat('MMM Do')}}, {{ \Carbon\Carbon::parse($venue->end_date)->isoFormat('YYYY')}} </h6>
                            <br>
                            <br>
                            <br>
                            <br>
                            <table width="80%" align="center">
                                <tr>
                                    <td><b>Name:</b> {{ \Auth::user()->name }}</td>
                                    <td><b>Contact No:</b> </td>
                                </tr>
                                <tr>
                                    <td><b>

                                        </b>

                                    </td>

                                    <td><b>Vehicle Model:</b> </td>
                                </tr>
                                <tr>
                                    <td><b>Receipt Type:</b> </td>
                                    <td><b>Receipt No:</b> </td>
                                </tr>
                            </table>

                        </div>
                        <button id="btn"> Print</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@section('javascript')

<script>
    $("#btn").click(function() {
        alert()
        printDiv()
        //$("#print-div").print();
    });

    function printDiv() {
        var printContents = document.getElementById("print-div").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

@endsection


@endsection
