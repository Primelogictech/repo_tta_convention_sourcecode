@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-md-12 d-none d-md-block py-2">
                <div class="text-center">
                    <ul class="list-unstyled events-tabslist d-inline-block">
                        @foreach($event->schedules as $schedule_date)
                        <li class="rightborder2">
                            <a class="event-tab {{$loop->first ? "first-date": "" }} " data-url="{{ env("APP_URL") }}/api/get-schedule-on-date/{{$schedule_date->id}}" target-id="#day_2_tab">{{ $event->from_date->format('D') }} , DAY {{$loop->iteration}} </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="row event_active_tab" id="day_1_tab">
            <div class="col-12 px-0 px-md-3">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="datatable table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    <th>S.no</th>
                                    <th>DAY</th>
                                    <th>TIME</th>
                                    <th>PROGRAM</th>
                                    <th>ROOM</th>
                                </tr>
                            </thead>
                            <tbody id='data-append'>
                                <!--  <tr>
                                    <td>1</td>
                                    <td rowspan="2"><strong>Thursday, July 4th 2019</strong></td>
                                    <td>7:00PM to 12:00AM</td>
                                    <td>Leadership Appreciation Dinner</td>
                                    <td>CRYSTAL/SAPPHIRE/RUBY</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>7:00PM to 11:00PM</td>
                                    <td>Young Adults Social Mixer (Matrimony)</td>
                                    <td>Coutyard (By Hyatt)</td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@section('javascript')


<script>
    $(document).ready(function() {
        $('.first-date').addClass("event-tab");
        $('.first-date').addClass("event_active_tab_btn");
        if($('.first-date').data('url') != undefined ){
            ajaxCall($('.first-date').data('url'), 'get', null, ShowDataInTable)
        }
    })
    $(".event-tab").click(function() {
        ajaxCall($(this).data('url'), 'get', null, ShowDataInTable)

        //active_tab
        $(".event_active_tab_btn").removeClass("event-tab");
        $(".event_active_tab_btn").removeClass("event_active_tab_btn");
        $(this).addClass("event-tab");
        $(this).addClass("event_active_tab_btn");
        /*
                 $(".event_active_tab").hide();
                 $(".event_active_tab").removeClass("event_active_tab");
                 $($(this).attr("target-id")).addClass("event_active_tab");
                 $($(this).attr("target-id")).show(); */
    });

    function ShowDataInTable(data) {
        console.log(data)
        $('#data-append').empty()
        for (let i = 0; i < data.length; i++) {

            $('#data-append').append('<tr><td >' + (i + 1) + ' </td>'

                +
                '<td > <strong> ' + data[i].date + ' </strong></td >' +


                '<td> ' + data[i].from_time + ' to ' + data[i].to_time + ' </td>' +
                '<td> ' + data[i].program_name + ' </td>' +
                '<td> ' + data[i].room_no + ' </td></tr>')
        }
    }
</script>

@endsection


@endsection
