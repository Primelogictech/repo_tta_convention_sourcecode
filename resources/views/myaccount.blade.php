@extends('layouts.user.base')
@section('content')


<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-30 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12">
                <h5>My Dashboard</h5>
            </div>
            <div class="col-12">
                <div class="card-body px-0">
                    <div class="table-responsive">
                        @if(Auth::user()->registration_type_id!=0)
                        <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    <th>Member ID</th>
                                    <th>Registration ID</th>
                                    <th>Member Details</th>
                                    <th width="150px" >Children Details</th>
                                    <th>Sponsorship Type</th>
                                    <th>Sponsorship Category</th>
                                    <th>Payment Details</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ Auth::user()->member_id }}</td>
                                    <td>{{ Auth::user()->registration_id }}</td>
                                    <td>
                                        <div class="py-1">
                                            <span>Name:</span><span> {{ ucfirst(Auth::user()->first_name) }} {{ ucfirst(Auth::user()->last_name) }}</span>
                                        </div>
                                        <div class="py-1">
                                            <span>Email:</span><span> {{ Auth::user()->email }}</span>
                                        </div>
                                        <div class="py-1">
                                            <span>Mobile:</span><span> {{ Auth::user()->phone_code }} {{ Auth::user()->mobile }} </span>
                                        </div>
                                    </td>
                                    <td> 
                                    <b>Age Below 6</b>: {{ Auth::user()->children_count['below_6']?? "0" }}
                                    <br>
                                    <b>Age 7 to 15 </b>: {{ Auth::user()->children_count['age_7_to_15']?? "0" }}
                                    <br>
                                    <b>Age 16 to 23 </b>: {{ Auth::user()->children_count['age_16_to_23']?? "0" }}
                                    
                                    </td>
                                    <td>{{ Auth::user()->registrationtype->name ?? "" }}</td>
                                    <td>
                                         {{  Auth::user()->categorydetails->donortype->name ?? ""}} ($ {{Auth::user()->total_amount }})</td>
                                    <td>
                                        <div>Paid Amount : ${{Auth::user()->amount_paid }}</div>
                                        <div>Due :
                                            @php
                                                $due=Auth::user()->total_amount -Auth::user()->amount_paid;
                                            @endphp    
                                        $ {{ $due }}</div>
                                        <div>Payment status: {{ucfirst(Auth::user()->payment_status) }}</div>
                                    </td>
                                    <td>
                                        <!--  <a href="registration.php" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a> -->
                                        @if(Auth::user()->registrationtype->name!='Family / Individual')
                                        <div>
                                            <a href="{{route('viewfeatures')}}" class="btn btn-sm btn-success text-white my-1 mx-1">View Features</a>
                                        </div>
                                         <div>
                                            <a href="{{url('bookticket')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Upgrade</a>
                                        </div>
                                           @endif
                                        <div>

                                        </div>
                                        @if($due>0)
                                        <div>
                                            <a href="{{route('pay-pending-amount')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Pay Pending Amount</a>
                                        </div>
                                        @endif
                                       
                                        <!-- <div>
                                            <a href="{{ url('print-ticket') }}" class="btn btn-sm btn-success text-white my-1 mx-1">View / Print Ticket</a>
                                        </div> -->
                                        <div>
                                            <a href="{{url('paymenthistory')}}" class="btn btn-sm btn-success text-white my-1 mx-1">View Payment History</a>
                                        </div>
                                      {{--   @forelse ($registrations as $registration)
                                        <div>
                                            <a href="{{url('show-registration-details',$registration->id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">View {{ $registration->registration_type_name  }} Registration</a>
                                        </div>
                                        @empty

                                        @endforelse --}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        @else
                        <h5> You have not Registered for the event Please click <a href="bookticket">here<a /> to register</h5>
                        @endif
                    </div>
                    Note: {!! Auth::user()->registration_note !!}
                </div>
            </div>
        </div>
    </div>
</section>



@section('javascript')



@endsection


@endsection
