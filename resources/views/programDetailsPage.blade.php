@extends('layouts.user.base')
@section('content')

<style type="text/css">
    p{
        font-size: 16px;
        margin-bottom: 20px;
    }
</style>
    

<section class="container-fluid my-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12 col-md-5 col-lg-4 my-1 px-0 px-md-3">
                <div class="p5 border">
                    <img src="{{asset(config('conventions.program_display').$program->image_url)}}" class="img-fluid w-100" alt="lakshmi kalayanam">
                </div>
            </div>
            <div class="col-12 col-md-7 col-lg-8 shadow-small px-4 py-3 my-1">
                <h5 class="text-danger">Contact Details:</h5>
                <div class="fs16">
                    <label>Chair Name:</label>

                    
                    <span class="text-skyblue">{{$program->chair_name}}</span>
                </div>
                <div class="fs16">
                    <label><i class="fas fa-mobile-alt"></i> :</label>
                    <span class="text-skyblue">{{$program->mobile_number}}</span>
                </div>
                <div class="fs16">
                    <label>Co-Chair Name:</label>
                    <span class="text-skyblue">{{$program->co_chair_name}}</span>
                </div>
            </div>
        </div>


     {!! $program->page_content  !!}



    </div>
</section>



@section('javascript')

@endsection


@endsection