         <div class="row mt-2">
             <div class="col-12">
                 <h5 class="text-violet py-2">Payment Types</h5>
                 <div>
                     @foreach ($RegistrationContent->patment_types as $patment_type)
                     <span class="mr-1 mr-md-5 position-relative pr-3">

                         <input type="radio" id="payment_{{$RegistrationContent->paymenttype( $patment_type)->name }}" class="p-radio-btn payment-button" data-name="{{$RegistrationContent->paymenttype( $patment_type)->name }}" value="{{$RegistrationContent->paymenttype( $patment_type)->id }}" name="payment_type" /><span class="fs16 pl-4 pl-md-4">{{ucfirst($RegistrationContent->paymenttype( $patment_type)->name )}}</span></span>


                     {{-- <span class="position-relative pr-3"><input type="radio" class="p-radio-btn" name="payment_type" /><span class="fs16 pl-4 pl-md-4">Cheque/Cash</span></span> --}}
                     @endforeach
                     <div class="payment-button error"></div>
                 </div>
             </div>
         </div>
         <hr class="dashed-hr">
         <div class="row">
             <!-- paypal payment -->

             <div class="col-12" id="paypal" style="display: none;">
                 <h5 class="text-violet mb-3">Payment by using Paypal</h5>
                 <div class="paypal-note">
                 </div>
                 <div class="form-row">
                 </div>
             </div>

             <!-- paypal payment end -->

             <div class="col-12" id="check_payment" style="display: none;">
                 <h5 class="text-violet mb-3">Payment by using check</h5>
                 <div class="check-note">
                 </div>
                 <div class="form-row">
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Cheque Number: </label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="cheque_number" id="" class="form-control cheque_form_field" placeholder="Cheque Number">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Cheque Date </label><span class="text-red">*</span>
                         <div>
                             <input type="date" name="cheque_date" id="" class="form-control cheque_form_field" placeholder="cheque Date">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Handed over to:</label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="more_info" id="" class="form-control cheque_form_field" placeholder="Handed over to">
                         </div>
                     </div>
                 </div>
             </div>

             <!-- zelle -->
             <div class="col-12" id="zelle" style="display: none;">
                 <h5 class="text-violet mb-3">Payment by using Zelle</h5>
                 <div class="zelle-note">
                 </div>
                 <div class="form-row">
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Zelle Reference Number</label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="Zelle_Reference_Number" id="" class="form-control zelle_form_field" placeholder="Zelle Reference Number">
                         </div>
                     </div>
                 </div>
             </div>
             <!-- zelle end -->
             <!-- othre Payments -->
             <div class="col-12" id="other" style="display: none;">
                 <h5 class="text-violet mb-3">Payment by using other payments</h5>
                 <div class="other-note">
                 </div>
                 <div class="form-row">
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Payment made through </label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="Payment_made_through" id="" class="form-control other_payment_form_field" placeholder="Payment made through">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Transaction Date </label><span class="text-red">*</span>
                         <div>
                             <input type="date" name="transaction_date" id="" class="form-control other_payment_form_field" placeholder="Transaction Date">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Transaction Id </label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="transaction_id" id="" class="form-control other_payment_form_field" placeholder="Transaction id">
                         </div>
                     </div>

                 </div>
             </div>
             <!-- othre Payments end -->

             @section('eaxtra-javascript')
             <script>
                 paymenttypes = ({!!html_entity_decode($paymenttypes) !!})
                 $(".payment-button").click(function(e) {
                     if ($(this).prop("checked")) {
                         if ($(this).data('name') == "{{ config('conventions.paypal_name_db') }}") {
                             $('#paypal').show();
                             $('#check_payment').hide();
                             $('#zelle').hide();
                             $('#other').hide();
                             paymenttypes.forEach(data => {
                                 if (data.name == "{{ config('conventions.paypal_name_db') }}") {
                                     $('.paypal-note').html(data.note)
                                 }
                             });
                             $('.cheque_form_field').prop("required", false);
                             $('.zelle_form_field').prop("required", false);
                             $('.other_payment_form_field').prop("required", false);



                         }

                         if ($(this).data('name') == "{{ config('conventions.check_name_db') }}") {
                             $('#check_payment').show();
                             $('#paypal').hide();
                             $('#zelle').hide();
                             $('#other').hide();

                             paymenttypes.forEach(data => {
                                 if (data.name == "{{ config('conventions.check_name_db') }}") {
                                     $('.check-note').html(data.note)
                                 }
                             });
                             $('.cheque_form_field').prop("required", true);
                             $('.zelle_form_field').prop("required", false);
                             $('.other_payment_form_field').prop("required", false);
                         }

                         if ($(this).data('name') == "{{ config('conventions.zelle_name_db') }}") {
                             $('#zelle').show();
                             $('#paypal').hide();
                             $('#check_payment').hide();
                             $('#other').hide();
                             paymenttypes.forEach(data => {
                                 if (data.name == "{{ config('conventions.zelle_name_db') }}") {
                                     $('.zelle-note').html(data.note)
                                 }

                             });
                             $('.zelle_form_field').prop("required", true);
                             $('.cheque_form_field').prop("required", false);
                             $('.other_payment_form_field').prop("required", false);

                         }
                         if ($(this).data('name') ==  "{{ config('conventions.other_name_db') }}") {

                             $('#other').show();
                             $('#zelle').hide();
                             $('#paypal').hide();
                             $('#check_payment').hide();
                             paymenttypes.forEach(data => {
                                 if (data.name == "{{ config('conventions.other_name_db') }}") {
                                     $('.other-note').html(data.note)
                                 }
                             });
                             $('.zelle_form_field').prop("required", false);
                             $('.cheque_form_field').prop("required", false);
                             $('.other_payment_form_field').prop("required", true);

                         }
                         //zelle
                     }
                 });
             </script>
             @endsection
