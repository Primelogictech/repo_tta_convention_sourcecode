@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-5">
    <div class="container shadow-small">
        <div class="row">
            <div class="col-12 px-0">
                <div class="leadership-heading-bg p-3">
                    <h4 class="mb-0">{{$LeadershipTypes->name}}</h4>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 py-4 p-md-4 p40">
                <div class="row">
                    @foreach($Leaders as $Leader)
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                            <div class="hexagon">
                                <div>
                                    <img src="{{asset(config('conventions.member_display'))}}/{{$Leader->member->image_url}}" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                                </div>
                                <div class="text-center my-2">
                                    <a href="#" class="leader-name">{{$Leader->member->name}}</a>
                                </div>
                                <div class="text-center mb10 text-white">{{$Leader->designation->name}}</div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>


@endsection
