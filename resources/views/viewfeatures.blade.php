@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12">
                <h5>Features</h5>
                <a href="{{route('myaccount')}}" data-toggle="tooltip" title="" class="float-right-back-btn btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
            <div class="col-12">
                <div class="card-body px-0">
                    <div class="table-responsive">
                        <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    <th>Sl NO.</th>
                                    <th>
                                        <div>Sponsor Category</div>
                                        <div>(Amount)</div>
                                    </th>
                                    <th style="min-width: 300px;">Benefits</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>
                                        <div>{{ $categorydetails->donortype->name }}</div>
                                        <div class="text-orange font-weight-bold pt-2">
                                        $ {{ $categorydetails->start_amount }} {{ $categorydetails->end_amount?  "- $ ".$categorydetails->end_amount :  "" }}
                                        </div>
                                    </td>
                                    <td>
                                        <ul class="list-unstyled pl20 mb-0">
                                            @foreach ($categorydetails->benfits as $benfit)
                                            <li class="py-1">{{ $loop->iteration}} .&nbsp;&nbsp;&nbsp;&nbsp;{{ $benfit->name  }} {{ ($benfit->pivot->count) }} </li>
                                        
                                             @if($benfit->has_image)
                                                    <a href="" id="anc_ben_img_{{ $benfit->id }}" target="blank"><img src="" id="ben_img_{{ $benfit->id }}"  width="50" height="50"></a>
                                            @else
                                                    <li class="py-1  " id="benfit_{{$benfit->id}}"><b>Assigned:</b> </li>
                                            @endif
                                            @if($benfit->has_image)
                                                    <form action="{{url('update_banner_image')}}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                        <input type="file" id="benfit_Image_{{$benfit->id}}" name="benfitimage[{{ $benfit->id }}]" accept="image/*"> 
                                                        <input type="hidden" name="benfit_id" value="{{$benfit->id}}" > 
                                                        <input type="submit" value="Update" >  
                                                    </form>
                                            @endif
                                                @endforeach
                                            </ul>
                                    </td>
                                  
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@section('javascript')
<script>
    benfits = JSON.parse('{!!json_encode($benfits) !!}')
    benfit_image_path= "{{asset(config('conventions.benfit_image_display'))}}"

    benfits.forEach(benfit => {
        $('#benfit_' + benfit.pivot.benfit_id).text(benfit.pivot.content)
    });
    bannerimages = JSON.parse('{!!json_encode($bannerimages) !!}')

    bannerimages.forEach(benfit => {
       $('#ben_img_'+ benfit.benfit_id).attr('src',benfit_image_path +'/' + benfit.image_url)
       $('#anc_ben_img_'+ benfit.benfit_id).attr('href',benfit_image_path +'/' + benfit.image_url)
    });

</script>
@endsection


@endsection
