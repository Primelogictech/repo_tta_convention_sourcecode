<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home Page</title>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!-- Bootstrap CSS -->

        <link href="{{ asset('css/bootstrapcss.css') }}" rel="stylesheet" />

        <!-- Fontawesome icons -->

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css" />

        <!-- Animations -->

        <link href="{{ asset('css/Animations.css') }}" rel="stylesheet" />

        <!-- Font family -->

        <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet" />

        <!--Datatables CSS-->

        <link href="{{ asset('css/datatables.css') }}" rel="stylesheet" />

        <!-- AOS CSS -->

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />

        <!--Extra CSS-->

        <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />

        <link href="{{ asset('css/tta.css') }}" rel="stylesheet" />

        <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet" />

        <link href="{{ asset('css/scrolling-tabs.css') }}" rel="stylesheet">
    </head>

    <style type="text/css">
        .left-logo-positions {
            position: absolute;
            left: 15px;
            top: 3px;
            z-index: 9;
            /*background-color: #00adef;
            border: 3px solid #3fc9fd !important;*/
            /*background-color: #92278f;
            border: 1px solid #bb25b7 !important;*/
        }
        .right-logo-positions {
            position: absolute;
            right: 15px;
            top: 3px;
            z-index: 9;
            /*background-color: #00adef;
            border: 3px solid #3fc9fd !important;*/
            /*background-color: #92278f;
            border: 1px solid #bb25b7 !important;*/
        }
        .bg-skyblue{
            /*background-color: #00adef;*/
            background-color: #92278f;
        }
        .bg-blue{
            background-color: #622595;
        }
    </style>


    <body>
        <!-- Header -->

        <header class="">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 d-none d-lg-block">
                        <!-- <div class="row bg-skyblue justify-content-center px-5">
                            <div class="col-lg-10 offset-lg-1 px-5 mx-5 py-1">
                                <div class="row">
                                    <div class="col-12 col-md-5 col-lg-5">
                                        <div class="text-center">
                                            <a href="#" class="text-white text-decoration-none fs15">Follow Us on: <i class="fab fa-facebook-f px-2"></i><i class="fab fa-twitter px-2"></i><i class="fab fa-youtube px-2"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-7 col-lg-7">
                                        <div class="text-center">
                                            <a href="#" class="text-white text-decoration-none helpline">Helpline : 1-866-TTA-SEVA (1-866-882-7382)</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="row bg-skyblue">
                            <article class="left-logo-positions">
                                <a href="{{url('/')}}">
                                    @if($data['left_logo'] !=null )
                                    <img src="{{asset(config('conventions.logo_display').$data['left_logo']->image_url)}}" alt="" border="0" class="img-fluid" width="130" />
                                @endif    
                                </a>
                            </article>
                            <div class="col-12 pt15 pb15">
                                <div class="text-center">
                                    <strong class="web-name text-white website-name">TELANGANA AMERICAN TELUGU ASSOCIATION</strong>
                                    <strong class="fs19 text-white d-block website-name">తెలుగు కళల తోట | తెలంగాణ సేవల కోట </strong>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center bg-blue"  data-toggle="sticky-onscroll">
                            <div>
                                <nav class="navbar navbar-expand-lg navbar-light top-navbar py-0 justify-content-center d-none d-lg-block">
                                    <!-- Navbar links -->

                                    <div class="collapse navbar-collapse" id="collapsibleNavbar">
                                        <ul class="navbar-nav">
                               <!-- <li class="nav-item active">
                                   <a class="nav-link lh20" href="{{url('/')}}">
                                       <img src="{{ asset('images/home.png')}}" alt="" />
                                   </a>
                               </li> -->

                               <li class="nav-item">
                                    <a class="nav-link" href="{{url('/')}}">Home</a>
                                </li>

                               <li class="nav-item dropdown">
                                   <a class="nav-link" href="">
                                       Registration
                                   </a>
                                   <div class="dropdown-menu submenu">
                                       <div class="row">
                                           <div class="col-md-12">
                                               <ul class="list-unstyled">
                                                   <li>
                                                       <a href="{{ url('bookticket') }}" class="">Convention Registration</a>
                                                   </li>
                                                   <li>
                                                       <a href="{{ url('exhibits-reservation') }}" class="">Exhibit Registration</a>
                                                   </li>
                                               </ul>
                                           </div>
                                       </div>
                               </li>


                               @foreach($data['menu'] as $menu)
                               <li class="nav-item   {{$menu->slug==null? 'dropdown': '' }}  ">
                                   <a class="nav-link" href="{{env("APP_URL")}}/{{$menu->slug==null ? '#': $menu->slug }}" id="navbardrop" {{$menu->slug==null? 'data-toggle="dropdown"': '' }}>
                                       {{$menu->name}}
                                   </a>
                                   <div class="dropdown-menu submenu">
                                       <div class="row">
                                           <div class="col-md-12">

                                               <ul class="list-unstyled">
                                                   @foreach($menu->submenus as $submenu)
                                                   <li>
                                                       <a href="{{env("APP_URL")}}/{{$submenu->slug }}" class="">{{$submenu->name}}</a>
                                                   </li>
                                                   @endforeach
                                               </ul>

                                           </div>


                                       </div>
                                   </div>
                               </li>
                               @endforeach

        
                               @guest
                               <li class="nav-item active">
                                   <a class="nav-link lh25 text-white" href="{{route('login')}}">
                                       Login
                                   </a>
                               </li>
                               @endguest

                               @auth
                               <li class="nav-item dropdown">
                                   <a class="nav-link py-1" href="#" id="navbardrop" data-toggle="dropdown">
                                       <img src="images/avtar1.png" alt="">
                                   </a>
                                   <div class="dropdown-menu user-icon submenu s-row">
                                       <div class="row">
                                           <div class="col-md-12">
                                               <ul class="list-unstyled">
                                                   <li>
                                                       <a href="{{ url('myaccount') }}">My Dashboard</a>
                                                   </li>
                                                   <li>
                                                       <form method="POST" action="{{ route('logout') }}">
                                                           @csrf

                                                           <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                                               {{ __('Log Out') }}
                                                           </x-dropdown-link>
                                                       </form>
                                                       <!--  <a href="#">Logout</a> -->
                                                   </li>
                                               </ul>
                                           </div>
                                       </div>
                                   </div>
                               </li>

                               @endauth

                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <article class="tabhorizontal-hide right-logo-positions r-p8">
                            <a href="{{url('/')}}">
                                @if($data['right_logo'] !=null )
                            <img src="{{asset(config('conventions.logo_display').$data['right_logo']->image_url)}}" class="img-fluid" width="117" border="0" alt="" />
                           @endif
                            </a>
                        </article>
                    </div>
                </div>
            </div>
        </header>

        <div class="container-fluid d-block d-lg-none header-bg" data-toggle="sticky-onscroll">
            <div class="row">
                <div class="col-12 px-0">
                    <div class="d-lg-flex text-white float-lg-right">
                        <div class="my-auto d-none">
                            <img src="images/location-icon1.png" class="my-auto pr-3" alt="" />
                        </div>
                        <div class="my-auto">
                            <label class="mb-0 text-white fs17 d-none d-lg-block">Location</label>
                            <marquee class="text-white text-center text-lg-left fs17">July 1st to 3rd, 2022 Renaissance Schaumburg Convention Center Hotel, 1551 Thoreau Dr N, Schaumburg, IL 60173</marquee>
                        </div>
                    </div>

                    <nav class="navbar-dark d-block d-lg-none bg-white py-2" data-toggle="sticky-onscroll">
                        <div class="navbar">
                            <!-- Brand -->

                            <a class="navbar-brand py-0" href="index.php">
                                <img src="images/logo.png" alt="" border="0" width="60" class="img-fluid" />
                            </a>

                            <div class="nav-item">
                                <a class="nav-link px-1" href="#">
                                    <span class="text-dark fs40 font-weight-bold" onclick="openNav()"><i class="fas fs30">&#xf039;</i></span>
                                </a>
                            </div>
                        </div>
                    </nav>
                    <div id="mySidenav" class="sidenav d-block d-lg-none">
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                        <ul class="navbar-nav">
                            <li class="sidemenu-nav-item sidemenu-active">
                                <a class="sidemnu-nav-link" href="#">
                                    <span>Home</span>
                                </a>
                            </li>
                            <li class="sidemenu-nav-item sidemenu-dropdown">
                                <a class="sidemnu-nav-link" href="#"> <span>Registration</span><i class="fas fa-angle-right float-right pt4"></i> </a>
                                <ul class="submenu d-none">
                                    <li><a href="#">Awards</a></li>
                                    <li><a href="business_conference_details.php">Business Conference</a></li>
                                    <li><a href="business_plan_competition.php">Business Plan Competition</a></li>
                                    <li><a href="page-under-constrution.php">CME</a></li>
                                    <li><a href="#">Convention</a></li>
                                    <li><a href="#">Cultural</a></li>
                                    <li><a href="exhibits_reservation.php">Exhibits</a></li>
                                    <li><a href="matrimony_registration.php">Matrimony</a></li>
                                    <li><a href="#">Miss. TTA &amp; Mrs. TTA</a></li>
                                    <li><a href="shathamanam_bhavathi_registration.php">Shathamanam Bhavathi</a></li>
                                    <li><a href="tta_got_talent.php">TTA Got Talent</a></li>
                                    <li><a href="women's_conference_details.php">Women's Conference</a></li>
                                    <li><a href="#">Youth Activities</a></li>
                                    <li><a href="lakshmi_narasimha_swami_kalyanam_details.php">Lakshmi Narasimha Swami Kalyanam</a></li>
                                    <li><a href="#">Souvenir Registrations</a></li>
                                </ul>
                            </li>
                            <li class="sidemenu-nav-item sidemenu-dropdown">
                                <a class="sidemnu-nav-link" href="#"> <span>Team</span><i class="fas fa-angle-right float-right pt4"></i> </a>
                            </li>
                            <li class="sidemenu-nav-item sidemenu-dropdown">
                                <a class="sidemnu-nav-link" href="#"> <span>Speakers</span><i class="fas fa-angle-right float-right pt4"></i> </a>
                            </li>
                            <li class="sidemenu-nav-item">
                                <a class="sidemnu-nav-link" href="#">
                                    <span>Exhibits</span>
                                </a>
                            </li>
                            <li class="sidemenu-nav-item">
                                <a class="sidemnu-nav-link" href="#">
                                    <span>Schedule</span>
                                </a>
                            </li>
                            <li class="sidemenu-nav-item sidemenu-dropdown">
                                <a class="sidemnu-nav-link" href="#"> <span>Donors</span><i class="fas fa-angle-right float-right pt4"></i> </a>
                            </li>
                            <li class="sidemenu-nav-item sidemenu-dropdown">
                                <a class="sidemnu-nav-link" href="page-under-constrution.php"> <span>Logistics</span><i class="fas fa-angle-right float-right pt4"></i> </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    
