<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <title>Home Page</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->

    <!-- <link href="{{ asset('css/bootstrapcss.css') }}" rel="stylesheet" /> -->

    <!-- Fontawesome icons -->

    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css" /> -->

    <!-- Animations -->

    <!-- <link href="{{ asset('css/Animations.css') }} " rel="stylesheet" /> -->

    <!-- Font family -->

    <!-- <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet" /> -->

    <!--Datatables CSS-->

    <!-- <link href="{{ asset('css/datatables.css') }}" rel="stylesheet" /> -->

    <!-- <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" /> -->

    <!--Custom CSS-->

    <!-- <link href="{{ asset('css/custom.css') }}" rel="stylesheet" /> -->

    <!-- <link href="{{ asset('css/nriva.css') }}" rel="stylesheet" /> -->

    <!-- <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet" /> -->

    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
</head>

<body> 
    @include('layouts.user.header')
    @yield('content')
    <!-- Page Content -->
   
    </div>
</body>

@include('layouts.user.footer')
@yield('javascript')

</html>