@extends('layouts.user.base')
@section('content')

<style type="text/css">
    th {
        border-color: #dadada !important;
    }
</style>

<section class="container-fluid my-3">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/exhibits-registration.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 col-md-5 col-lg-4">
                                <h5 class="text-violet">Contact For More Details</h5>
                                <h6 class="">Jayaprakash Madhamshetty</h6>
                                <div class="py-1 my-auto"><i class="fas fa-mobile-alt text-skyblue fs18"></i><span class="icon-positions fs16 lh16">(313)-XXX-XXXX</span></div>
                                <div class="py-1 my-auto"><i class="far fa-envelope text-skyblue fs18"></i><a href="#" class="icon-positions text-black text-decoration-none fs16 lh16">exhxxxxxxxxxx@nriva.org</a></div>
                            </div>
                            <div class="col-12 col-md-7 col-lg-8 my-2 my-md-0">
                                <div class="text-md-right">
                                    <a href="#exhibit-registration" class="btn btn-danger btn-lg text-uppercase mr-2 my-1 fs-xs-15">Exhibit Registration</a>
                                    <a href="documents/2019-Detroit-Convention-Exhibit_Booths_Package_RE.pdf" target="_blank" class="btn btn-danger btn-lg text-uppercase my-1 fs-xs-15">Exhibits Package Details</a>
                                </div>
                            </div>
                            <div class="col-12 mt-3">
                                <h5 class="text-orange">Timings</h5>
                                <h6 class="text-skyblue">Booths Setup</h6>
                                <ul class="list-unstyled exhibits-list pl20 pt10 fs16">
                                    <li>Thursday July 4th, 2019 – 9:00 A.M. to 5:00 P.M EST</li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h6 class="text-skyblue">Hours</h6>
                                <ul class="list-unstyled exhibits-list pl20 pt10 fs16">
                                    <li>Friday - July 5th, 2019 - 10:00 A.M. to 11:00 P.M. EST</li>
                                    <li>Saturday - July 6th, 2019 - 10:00 A.M. to 11:00 P.M. EST</li>
                                    <li>Sunday - July 7th, 2019 - 10:00 A.M. to 02:00 P.M. EST</li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h6 class="text-skyblue">Tear Down</h6>
                                <ul class="list-unstyled exhibits-list pl20 pt10 fs16">
                                    <li>Sunday – July 7th, 2019 - 04:00 p.m. to 06:00 p.m. EST</li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h6 class="text-skyblue">Questions?</h6>
                                <ul class="list-unstyled exhibits-list pl20 pt10 fs16">
                                    <li>
                                        Call JP Madhamsetty at +1 (313)-XXX-XXXX (or) email:
                                        <a class="text-black text-decoration-none" href="#"> exhxxxxxxxxxx@nriva.org</a>
                                    </li>
                                    <li>
                                        Call Suresh Cholaveti at +1 (614)-XXX-XXXX (or) email:
                                        <a class="text-black text-decoration-none" href="#"> exhxxxxxxxxxx@nriva.org</a>
                                    </li>
                                    <li>
                                        <a class="text-black text-decoration-none" href="#">Call Srinivas Chittaluri at +1 (848)-XXX-XXXX (or) email:</a>
                                        <a class="text-black text-decoration-none" href="#"> exhxxxxxxxxxx@nriva.org</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h6 class="text-skyblue">What is Included</h6>
                                <p class="mb-0 fs16">
                                    All booths are of 10' x 10' size and include the following. For a bigger booth of 20' x 10', you will get two sets of these items.
                                </p>
                                <ul class="list-unstyled exhibits-list pl20 pt10 fs16">
                                    <li>8’ High Back Wall.</li>
                                    <li>3’ High Side Drape.</li>
                                    <li>7" x 44" One-line ID Sign.</li>
                                    <li>6’ Long Skirted Table - 1 No.</li>
                                    <li>Side Chairs - 2 Nos.</li>
                                    <li>Waste Basket/Trash Can - 1.</li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h5 class="text-violet">Optional Accessories</h5>
                                <p class="mb-0 fs16">
                                    Optional Accessories/Items are available for additional pricing. Please contact us for pricing.
                                </p>
                            </div>
                            <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 my-3">
                                <h5 class="text-violet text-center">Exhibit Booths Pricing</h5>
                                <div class="card-body px-0">
                                    <div class="table-responsive">
                                        <table class="table-bordered table table-hover table-center mb-0">
                                            <tr>
                                                <th align="center" rowspan="2" class="bg-violet text-white text-center">S.No</th>
                                                <th align="center" rowspan="2" class="bg-violet text-white text-center"><strong>Exhibitor Type</strong></th>
                                                <th align="center" rowspan="2" class="bg-violet text-white text-center"><strong>Size</strong></th>
                                                <th align="center" class="bg-violet text-white text-center"><strong>Before</strong></th>
                                                <th align="center" class="bg-violet text-white text-center"><strong>After</strong></th>
                                            </tr>
                                            <tr>
                                                <th align="center" class="bg-violet text-white text-center">
                                                    <strong>{{ \Carbon\Carbon::parse($Venue->Event_date_time)->subDays(1)->isoFormat('MM/DD/YYYY')}}</strong>


                                                </th>
                                                <th align="center" class="bg-violet text-white text-center">
                                                    <strong>{{ \Carbon\Carbon::parse(($Venue->Event_date_time))->subDays(1)->isoFormat('MM/DD/YYYY')}}</strong>


                                                </th>
                                            </tr>
                                            <tbody>

                                                @foreach ($exhibitsTypes as $exhibitsType )


                                                @foreach($exhibitsType->size_price as $size_price)
                                                <tr>
                                                    <td align="center">{{ $loop->parent->iteration }}</td>
                                                    <td class="bg-gold text-white">{{ $exhibitsType->name }}</td>
                                                    <td>{{ $size_price['size'] }}</td>
                                                    <td>${{ $size_price['price_before']}} ({{ \Carbon\Carbon::parse($size_price['till_date'])->isoFormat('MM/DD/YYYY')}}) </td>
                                                    <td>${{ $size_price['price_after'] }} ({{ \Carbon\Carbon::parse($size_price['till_date'])->isoFormat('MM/DD/YYYY')}}) </td>
                                                </tr>

                                                @endforeach

                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <h6 class="text-danger">Note:</h6>
                                <div class="fs16">Premium (outside) booths are extra. Please contact for pricing</div>
                                <div class="fs16">Cancellation Policy: <strong>50% Refund before June 01, 2019</strong> &amp; Nonrefundable thereafter.</div>
                            </div>
                            <div class="col-12 mt-5" id="exhibit-registration">
                                <h4 class="text-violet text-center mb-3">Exhibits Registration</h4>
                                <h6 class="font-weight-bold text-center mb-3">Convention Registration is a must for any of the Event Registration.</h6>
                                <h6 class="font-weight-bold text-center mb-3">Please make sure that you have registered for the Convention before you register for this Event.</h6>

                                <div class="row px-3 px-lg-0">
                                    <div class="col-12 col-md-12 col-lg-10 offset-lg-1 shadow-small p-3 py-md-5 px-md-4 my-3">
                                        <form action="{{url('exhibits-reservation')}}" method="post" id="form" enctype="multipart/form-data">
                                            <div class="form-row">
                                                @csrf
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Full Name:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="full_name" value="{{Auth::user()->first_name}} {{Auth::user()->late_name}}" id="" class="form-control " placeholder="Full Name" />

                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Company Name:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="company_name" id="" class="form-control" placeholder="Company Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Category:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="category" class="form-control" placeholder="Category" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Tax ID:</label>
                                                    <div>
                                                        <input type="text" name="tax_ID" class="form-control" placeholder="Tax ID" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Mailing Address:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="mailing_address" id="" class="form-control" placeholder="Mailing Address" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Phone No:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="number" name="phone_no" id="" class="form-control" placeholder="Phone No" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Email Id:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="email" name="email" id="email" class="form-control" placeholder="Email Id" />

                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Fax: </label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="fax" id="" class="form-control" placeholder="Fax" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Name of your Website:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="website_url" id="website_url" class="form-control" placeholder="Name of your Website" />

                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Booth Type:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <select name="booth_type" class="form-control" id="booth_type" required>
                                                            <option value="" selected>Select </option>
                                                            @foreach($exhibitsTypes as $exhibitsType)
                                                            <option value="{{ $exhibitsType->id }}">{{ $exhibitsType->name }} </option>
                                                            @endforeach
                                                        </select>
                                                        {{-- <input type="text" name="booth_type" id="" class="form-control" placeholder="Booth Type" /> --}}
                                                    </div>
                                                </div>
                                            </div>
                                            @include('partalles.paymentForm')

                                            {{-- <input type="text" name="amount" id="amount" class="form-control" placeholder="amount" />
 --}}
                                            <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                                <label>Amount </label><span class="text-red">*</span>
                                                <input type="number" readonly name="amount" id="amount" class="form-control" placeholder="Amount">
                                                <div>
                                                    <input type="submit" class="btn btn-lg btn-danger text-uppercase px-5 submit-button" value="Check out">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 mt-3">
                                <div>
                                    <img src="images/Exhibits-Flyer.jpeg" class="img-fluid mx-auto d-block" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@section('javascript')
<script>
    $(document).ready(function() {
        exhibitsTypes = JSON.parse('{!! $exhibitsTypes !!}')

        function isGreaterThenTodatDate(date) {
            today = new Date("{{date('m/d/Y')}}")
            date = new Date(date)
            if (today >= date) {
                return false
            } else {
                return true
            }
        }

        $('#booth_type').change(function() {
            booth_type = $('#booth_type').val()
            price = 0;
            for (let i = 0; i < exhibitsTypes.length; i++) {
                if (exhibitsTypes[i].id == booth_type) {
                    data = exhibitsTypes[i].size_price
                    data.forEach(element => {
                        if (isGreaterThenTodatDate(element.till_date)) {
                            price = price + parseInt(element.price_before)
                        } else {
                            price = price + parseInt(element.price_after)
                        }
                    });
                }
            }
            $('#amount').val(price)
        })

        $("#form").validate({
            rules: {
                full_name: "required",
                company_name: "required",
                category: "required",
                tax_ID: "required",
                mailing_address: "required",
                phone_no: "required",
                email: "required",
                fax: "required",
                website_url: "required",
                booth_type: "required",
                payment_type: {
                    required: true
                },
            }
        });
    });
</script>

@endsection


@endsection
