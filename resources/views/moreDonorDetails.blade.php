@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12">
                <div class="text-violet fs22">{{$Donortype->name}}</div>
            </div>
            @foreach($donors as $donor)

            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="{{asset(config('conventions.member_display'))}}/{{$donor->member->image_url}}" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">{{$donor->member->name}}</h6>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</section>


@endsection
