@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">{{ucfirst($type)}} Logo</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                @if($logo == null)
                <a href="{{route($type.'-logo.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>

                            <tr>
                                <th>Sl NO.</th>
                                <th>Logo Name</th>
                                <th style="width:300px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($logo != null)
                            <tr>
                                <td>1</td>
                                <td>
                                    <div>
                                        <img src="{{asset(config('conventions.logo_display').$logo->image_url)}}" alt="{{$logo->name}}" width="200" class="img-fluid">
                                    </div>
                                </td>
                                <td>
                                    <a href="{{route($type.'-logo.edit', $logo->id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <button href="#" data-id={{$logo->id}} data-url='{{ env("APP_URL") }}/admin/{{$type}}-logo/{{$logo->id}}' class="btn btn-sm btn-success my-1 mx-1  delete-btn">Delete</button>
                                    <button href="#" data-url="{{ env("APP_URL") }}/admin/{{$type}}-logo-status-update" class="btn btn-sm btn-success text-white my-1 mx-1 status-btn" data-id={{$logo->id}}>{{ $logo->status=='1' ?  'Active': 'InActive' }}</button>
                                </td>
                            </tr>
                            @else
                            <p>NO logo Uploaded</p>
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
