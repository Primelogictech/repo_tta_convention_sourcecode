@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit {{ucfirst($type)}}</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route($type.'-logo.update', $logo->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Upload Logo <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="file" class="form-control" name="image_url">
                            <b>Image should be in 1:1 ration eg: 200X200</b>
                            <input type="hidden" class="form-control" name="type" value="left">
                        </div>
                        <div>
                            <img src="{{asset(config('conventions.logo_display').$logo->image_url)}}" alt="{{$logo->name}}" width="500" class="img-fluid">
                        </div>
                    </div>


                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')


@endsection

@endsection
