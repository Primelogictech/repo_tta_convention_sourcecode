@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Registrations</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">

        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>Sl NO.</th>
                                <th>Member ID</th>
                                <th>Registration ID</th>
                                <th>Member Details</th>
                                <th>Children Details</th>
                                <th>Sponsorship Type</th>
                                <th>Sponsorship Category</th>
                                <th>Amount Paid</th>
                                <th>Payment Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($users as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td> {{ $user->member_id }}</td>
                                <td> {{ $user->registration_id }}</td>
                                <td>
                                    <div class="py-1">
                                        <span>Name:</span><span> {{ $user->first_name }} {{ $user->last_name }}</span>
                                    </div>
                                    <div class="py-1">
                                        <span>Email:</span><span>{{ $user->email }}</span>
                                    </div>
                                    <div class="py-1">
                                        <span>Mobile:</span><span>{{ $user->phone_code }} {{ $user->mobile }}</span>
                                    </div>
                                </td>
                                <td>
                                <b>Age Below 6</b>: {{ $user->children_count['below_6']?? "0" }}
                                    <br>
                                    <b>Age 7 to 15 </b>: {{ $user->children_count['age_7_to_15']?? "0" }}
                                    <br>
                                    <b>Age 16 to 23 </b>: {{ $user->children_count['age_16_to_23']?? "0" }}
                                    
                                </td>
                                <td>{{ $user->registrationtype->name ?? "" }}</td>
                                <td> {{ $user->categorydetails->donortype->name ?? ""}} {{ isset($user->categorydetails->start_amount) ? ( '($'. $user->categorydetails->start_amount . '- $'. $user->categorydetails->end_amount . ")")  : ""  }} </td>
                                <td> {{ isset($user->amount_paid) ? '$'. $user->amount_paid  : ""  }}</td>
                                <td>{{ ucfirst($user->payment_status) }}</td>
                                <td>
                                   <!--  <a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a> -->
                                @if($user->registration_type_id!=0)

                                        @if($user->sponsorship_category_id!=0)
                                        <a href="{{ url('admin/assigne-features',$user->id) }}" class="btn btn-sm btn-success my-1 mx-1">Assign Features</a>
                                        @endif

                                        <a href="{{ url('admin/add-note',$user->id) }}" class="btn btn-sm btn-success my-1 mx-1">Add Note</a>
                                        <a href="{{ url('admin/payment-history',$user->id) }}" class="btn btn-sm btn-success my-1 mx-1">Payment History</a>
                                @endif
                                </td>
                            </tr>
                            @empty

                            @endforelse


                            <!--  <tr>
                                <td>2</td>
                                <td>23456</td>
                                <td>
                                    <div class="py-1">
                                        <span>Name:</span><span> Dinesh</span>
                                    </div>
                                    <div class="py-1">
                                        <span>Email:</span><span> dinesh@gmail.com</span>
                                    </div>
                                    <div class="py-1">
                                        <span>Mobile:</span><span> 9230447709</span>
                                    </div>
                                </td>
                                <td>Donor</td>
                                <td>Diamond ($5000)</td>
                                <td>$5000</td>
                                <td>Full amount paid</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <a href="assign_features.php" class="btn btn-sm btn-success my-1 mx-1">Assign Features</a>
                                </td>
                            </tr> -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
