@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Exhibit Registrations</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">

        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <tr>
                            <th>Sl NO.</th>
                            <th>Full Name</th>
                            <th>Email Id</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($registrations as $registration)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $registration->full_name }}</td>
                                <td>{{ $registration->email }}</td>
                                <td>
                                  <!--   <a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
                                    <a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>
                                    <a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a> -->
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
