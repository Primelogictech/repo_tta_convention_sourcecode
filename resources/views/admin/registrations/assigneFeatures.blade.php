@extends('layouts.admin.base')
@section('content')


<style type="text/css">
    input.form-control {
        height: 30px;
        width: 300px;
    }
</style>

<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Features</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{url('admin\registrations')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>Sl NO.</th>
                                <th>
                                    <div>Sponsor Category</div>
                                    <div>(Amount)</div>
                                </th>
                                <th style="min-width: 300px;">Benefits</th>
                                <th>Benfit Image</th>
                                <th style="min-width: 500px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($user->categorydetails->benfits as $benfits)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                @if($loop->first)
                                <td rowspan="{{$loop->count}}">
                                    <div>{{ $user->categorydetails->donortype->name }}</div>
                                    <div class="text-orange font-weight-bold pt-2">$ {{ isset($user->categorydetails->start_amount) ? ( '($'. $user->categorydetails->start_amount . '- $'. $user->categorydetails->end_amount . ")")  : ""  }}</div>
                                </td>
                                @endif
                                <td>
                                    {{ $benfits->name }} {{ $benfits->pivot->count>0 ?   '('.$benfits->pivot->count .')' : ""}}
                                </td>
                                <td>
                                    @if($benfits->has_image)
                                         <img src="" id="ben_img_{{ $benfits->id }}"  width="50" height="50">
                                         <a href="" id="ben_img__downlode_{{ $benfits->id }}" >Downlode</a>
                                    @endif
                                </td>
                                <td>
                                    @if($benfits->has_input)
                                    <div class="d-flex">
                                        <span class="my-1 mr-1"><input type="text" id="input_{{$benfits->id}}" class="form-control"></span>
                                        <button data-url="{{ env("APP_URL") }}/admin/assigne-features-update" data-user_id="{{ $user->id }}" data-id="{{$benfits->id}}" class="save-btn btn btn-sm btn-success my-auto mx-1">Save / Update</button>
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script>
    benfits = JSON.parse('{!! json_encode($user_benfits) !!}')
    benfits.forEach(benfit => {
        $('#input_' + benfit.id).val(benfit.pivot.content)
    });

    benfit_image_path= "{{asset(config('conventions.benfit_image_display'))}}"
    path= "{{url('')}}"
    bannerimages = JSON.parse('{!!json_encode($bannerimages) !!}')
     bannerimages.forEach(benfit => {
       $('#ben_img_'+ benfit.benfit_id).attr('src',benfit_image_path +'/' + benfit.image_url)
       $('#ben_img__downlode_'+ benfit.benfit_id).attr('href',path+'/admin/downlode-banner-image/'+benfit.id)
    });

    $('.save-btn').click(function() {
        id = $(this).attr('data-id')
        user_id = $(this).attr('data-user_id')

        content = $('#input_' + id).val()
        data = {
            "_token": $('meta[name=csrf-token]').attr('content'),
            id: $(this).attr('data-id'),
            user_id: user_id,
            content: content
        }
        ajaxCall($(this).data('url'), 'put', data, aftersave)
    })

    function aftersave(data) {
        location.reload();
    }
</script>


@endsection


@endsection
