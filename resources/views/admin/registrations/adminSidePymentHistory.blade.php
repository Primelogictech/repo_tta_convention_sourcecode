@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Registrations</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url('admin/registrations') }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>S No</th>
                                <th>Transaction details</th>
                                <th>Payment Methord</th>
                                <th>Amount</th>
                                <th>Payment Status</th>
                                <th>Account Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($payments as $payment)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                 <td>
                                     <b>Date</b>
                                     {{ $payment->created_at }}
                                    <br>
                                    <b>Transaction Id </b>
                                    {{ $payment->unique_id_for_payment }}
                                  
                                  <!--   <b>Transaction Id </b>{{ $payment->unique_id_for_payment }} -->
                                </td>
                                <td>{{ ucfirst($payment->paymentmethord->name)  }}</td>
                                <td> $ {{ $payment->payment_amount }} </td>
                                <td> {{ ucfirst($payment->payment_status) }} </td>
                                <td> {{ ucfirst($payment->account_status) }} </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
