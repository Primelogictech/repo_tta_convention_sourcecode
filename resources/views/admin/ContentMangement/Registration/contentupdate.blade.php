@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit RegistrationContent Page Content</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('registration-page-content-update', $RegistrationContent->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Additional Donor Benefits<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <textarea name="additional_donor_benefits">{{ $RegistrationContent->additional_donor_benefits }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Note<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <textarea name="note">{{ $RegistrationContent->note }}</textarea>
                        </div>
                    </div>




                    @foreach ($paymenttypes as $paymenttype)

                    <div class="form-group row">
                        <input type="checkbox" {{ in_array($paymenttype->id,$RegistrationContent->patment_types ) ? 'checked' : ""   }} name="patment_types[]" value="{{ $paymenttype->id }}">
                        <label class="col-form-label col-md-4 my-auto">{{ ucfirst($paymenttype->name) }}<span class="mandatory">*</span></label>

                    </div>

                    <div class="col-md-8 my-auto">
                        <textarea class="note" name="payment_note[{{ $paymenttype->id }}]">{{ $paymenttype->note }} </textarea>
                    </div>
                    @endforeach
            <br>
            <br>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Additional Information<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <textarea name="check_payable_to">{{ $RegistrationContent->check_payable_to }}</textarea>
                        </div>
                    </div>

                    <!--  <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Patment Types<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <textarea name="page_content"></textarea>
                        </div>
                    </div> -->

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>

<script>
    $(document).ready(function() {

        CKEDITOR.replace('additional_donor_benefits');
        CKEDITOR.replace('note');
        CKEDITOR.replace('check_payable_to');

        @foreach($paymenttypes as $paymenttype)

        CKEDITOR.replace('payment_note[{{$paymenttype->id}}]');
        @endforeach

    })
</script>
@endsection

@endsection
