@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Donors</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('donor.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>Sl NO.</th>
                                <th>Donor Name</th>
                                <th>Upload Photo</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($donors as $donor)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$donor->donor_name}}</td>
                                <td>
                                    <div>
                                        <img src="{{asset(config('conventions.donor_display').$donor->image_url)}}" alt="{{$donor->name}}" class="img-fluid">
                                    </div>
                                </td>
                                <td>
                                    <a href="{{route('donor.edit', $donor->id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <button href="#" data-id={{$donor->id}} data-url='{{ env("APP_URL") }}/admin/donor/{{$donor->id}}' class="btn btn-sm btn-success my-1 mx-1  delete-btn">Delete</button>
                                    <button href="#" data-url="{{env("APP_URL") }}/admin/donor-status-update" class="btn btn-sm btn-success text-white my-1 mx-1 status-btn" data-id={{$donor->id}}>{{ $donor->status=='1' ?  'Active': 'InActive' }}</button>
                                </td>
                            </tr>
                            @empty
                            <p>No donortypes to Show</p>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection