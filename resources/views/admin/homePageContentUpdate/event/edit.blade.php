@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit Event</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('event.update', $event->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Event Name <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$event->event_name}}" name="event_name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Event From Date<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="date" class="form-control" min="{{ $venue->Event_date_time->format('Y-m-d') }}" max="{{ $venue->end_date->format('Y-m-d') }}" value="{{ \Carbon\Carbon::parse($event->from_date)->isoFormat('Y-MM-DD')}}" name="from_date">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Event To Date<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="date" class="form-control" min="{{ $venue->Event_date_time->format('Y-m-d') }}" max="{{ $venue->end_date->format('Y-m-d') }}" value="{{ \Carbon\Carbon::parse($event->to_date)->isoFormat('Y-MM-DD')}}" name="to_date">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Description<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <textarea name="description">{{$event->description}}</textarea>
                        </div>
                    </div>



                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Upload Event Image <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="file" class="form-control" name="image">
                            <b>Image should be in 2:1 ration eg: 370X185</b>
                        </div>
                    </div>
                    <img src="{{asset(config('conventions.event_display').$event->image_url)}}" alt="event" width=100 class="img-fluid">
                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')

<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>


<script>
    CKEDITOR.replace('description');
</script>


@endsection

@endsection
