@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Events</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('event.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>Sl NO.</th>
                                <th>Name</th>
                                <th>From - To Dates</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($events as $event)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$event->event_name}}</td>
                                <td>{{ \Carbon\Carbon::parse($event->from_date)->isoFormat('MMM Do YYYY')}} To {{ \Carbon\Carbon::parse($event->to_date)->isoFormat('MMM Do YYYY')}} </td>
                                <td>
                                    <a href="{{route('event.edit', $event->id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <button href="#" {{$event->schedules_count==0 ? '': 'disabled' }} data-id={{$event->id}} data-url='{{ env("APP_URL") }}/admin/event/{{$event->id}}' class="btn btn-sm btn-success my-1 mx-1  delete-btn">Delete</button>
                                    <a href="{{url('admin/schedule/create')}}?event={{$event->id}}" class="btn btn-sm btn-success my-1 mx-1 ">Add schedule</a>
                                     <button href="#" data-url="{{ env("APP_URL") }}/admin/event-status-update" class="btn btn-sm btn-success text-white my-1 mx-1 status-btn" data-id={{$event->id}}>{{ $event->status=='1' ?  'Active': 'InActive' }}</button>
                                </td>
                            </tr>
                            @empty
                            <p>No donortypes to Show</p>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@section('javascript')


@endsection



@endsection
