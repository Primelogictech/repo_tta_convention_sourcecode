<?php

use App\Http\Controllers\Admin\HomePage\BannerController;
use App\Http\Controllers\Admin\HomePage\VenueController;
use App\Http\Controllers\Admin\HomePage\MessageController;
use App\Http\Controllers\Admin\Menu\MenuController;
use App\Http\Controllers\Admin\Menu\SubMenuController;
use App\Http\Controllers\Admin\Menu\PageController;
use App\Http\Controllers\Admin\DesignationController;
use App\Http\Controllers\Admin\DonortypeController;
use App\Http\Controllers\Admin\InviteeController;
use App\Http\Controllers\Admin\LeadershipTypeController;
use App\Http\Controllers\Admin\SponsorCategoryTypeController;
use App\Http\Controllers\Admin\LeftLogoController;
use App\Http\Controllers\Admin\RightLogoController;
use App\Http\Controllers\Admin\HomePage\ProgramController;
use App\Http\Controllers\Admin\HomePage\EventController;
use App\Http\Controllers\Admin\HomePage\ScheduleController;
use App\Http\Controllers\Admin\HomePage\DonorController;
use App\Http\Controllers\Admin\HomePage\VideoController;
use App\Http\Controllers\Admin\HomePage\MemberController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\AdminRegistrationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\Admin\PaymenttypeController;
use App\Http\Controllers\Admin\SponsorCategoryController;
use App\Http\Controllers\Admin\PaymentController;
use App\Http\Controllers\Admin\BenefittypeController;
use App\Http\Controllers\Admin\CommitteController;
use App\Http\Controllers\Admin\ExhibitorTypeController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'] );


Route::prefix('admin')->middleware(['admin', 'auth'])->group(function () {

    Route::get('/', [DashboardController::class, 'index'] );

    Route::resource('banner', BannerController::class);
    Route::put('banner-status-update', [BannerController::class ,'updateStatus']);

    Route::resource('venue', VenueController::class);

    Route::resource('message', MessageController::class);
    Route::put('message-status-update', [MessageController::class , 'updateStatus']);

    Route::resource('menu', MenuController::class);
    Route::put('menu-status-update', [MenuController::class, 'updateStatus']);

    Route::resource('submenu', SubMenuController::class);
    Route::put('submenu-status-update', [SubMenuController::class, 'updateStatus']);
    Route::get('get-submenu-with-menu/{id}/{edit?}', [SubMenuController::class, 'GetSubmenuWithMenu']);

    Route::resource('page', PageController::class);
    Route::put('page-status-update', [PageController::class, 'updateStatus']);

    Route::resource('donor', DonorController::class);
    Route::put('donor-status-update', [DonorController::class, 'updateStatus']);



    Route::resource('video', VideoController::class);
    Route::put('video-status-update', [VideoController::class, 'updateStatus']);

    Route::resource('member', MemberController::class);
    Route::put('member-status-update', [MemberController::class, 'updateStatus']);
    Route::get('assigne-roles/{id}', [MemberController::class, 'showAssigneRolesPage']);
    Route::post('store-assigne-roles', [MemberController::class, 'storeAssigneRolesPage']);

    // master routes
    Route::resource('designation', DesignationController::class);
    Route::put('designation-status-update', [DesignationController::class, 'updateStatus']);

    Route::resource('donortype', DonortypeController::class);
    Route::put('donortype-status-update', [DonortypeController::class, 'updateStatus']);

    Route::resource('invitee', InviteeController::class);
    Route::put('invitee-status-update', [InviteeController::class, 'updateStatus']);

    Route::resource('leadership-type', LeadershipTypeController::class);
    Route::put('leadership-type-status-update', [LeadershipTypeController::class, 'updateStatus']);

    Route::resource('sponsor-category-type', SponsorCategoryTypeController::class);
    Route::put('sponsor-category-type-status-update', [SponsorCategoryTypeController::class, 'updateStatus']);

    Route::resource('left-logo', LeftLogoController::class);
    Route::put('left-logo-status-update', [LeftLogoController::class, 'updateStatus']);
    Route::resource('right-logo', RightLogoController::class);
    Route::put('right-logo-status-update', [RightLogoController::class, 'updateStatus']);

    Route::resource('program', ProgramController::class);
    Route::put('program-status-update', [ProgramController::class, 'updateStatus']);

    Route::resource('event', EventController::class);
    Route::put('event-status-update', [EventController::class, 'updateStatus']);

    Route::resource('schedule', ScheduleController::class);

    Route::resource('sponsor-category', SponsorCategoryController::class);
    Route::put('sponsor-category-status-update', [SponsorCategoryController::class, 'updateStatus']);

    Route::resource('paymenttype', PaymenttypeController::class);
    Route::put('paymenttype-status-update', [PaymenttypeController::class, 'updateStatus']);

    Route::resource('benefittype', BenefittypeController::class);
    Route::put('benefittype-status-update', [BenefittypeController::class, 'updateStatus']);

    Route::resource('committe', CommitteController::class);
    Route::put('committe-status-update', [CommitteController::class, 'updateStatus']);

    Route::resource('exhibitor-type', ExhibitorTypeController::class);
    Route::put('exhibitor-type-status-update', [ExhibitorTypeController::class, 'updateStatus']);



    Route::get('registrations', [RegistrationController::class, 'showRegistrations']);
    Route::get('assigne-features/{id}', [RegistrationController::class, 'showAssigneFeatures']);
    Route::put('assigne-features-update', [RegistrationController::class, 'updateAssigneFeatures']);
    Route::get('exhibit-registrations', [AdminRegistrationController::class, 'exhibitRegistrations']);
    Route::get('youth-activities-registrations', [AdminRegistrationController::class, 'youthActivitiesRegistrations']);

    Route::get('add-note/{id}', [RegistrationController::class, 'showAddNoteForm']);
    Route::post('add-note', [RegistrationController::class, 'storeAddNoteForm'])->name('registration.addnote');

    Route::get('payment-history/{user_id}', [RegistrationController::class, 'adminSidePaymentHistory']);
    
    Route::get('downlode-banner-image/{id}', [RegistrationController::class, 'download']);
    
});

Route::get('reload-captcha', [HomeController::class, 'reloadCaptcha']);

Route::get('payment', [PaymentController::class, 'index']);
Route::get('payment/status', [PaymentController::class, 'status'])->name('payment.status');
Route::get('paypal/status', [PaymentController::class, 'paypalStatus'])->name('paypal.status');
Route::get('padingpayment/status', [PaymentController::class, 'pendingstatus'])->name('pendingpayment.status');
Route::get('print-ticket', [RegistrationController::class, 'printTicket']);

/* Route::get('/admin', function () {
    return view('admin/dashboard');
}); */
/* Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');
 */

require __DIR__.'/auth.php';

Route::middleware(['auth'])->group(function () {
    Route::get('viewfeatuees', [RegistrationController::class, 'viewFeatures'])->name('viewfeatures');
    Route::get('paymenthistory', [RegistrationController::class, 'PaymentHistory'])->name('PaymentHistory');
    Route::get('pay-pending-amount', [PaymentController::class, 'payPendingAmount'])->name('pay-pending-amount');
    Route::post('pay-pending-amount', [PaymentController::class, 'storePayPendingAmount'])->name('pay-pending-amount');
    Route::get('myaccount', [RegistrationController::class, 'myaccount'])->name('myaccount');
    Route::get('bookticket', [RegistrationController::class, 'ShowRegistrationForm'])->name('bookticket');
    Route::get('exhibits-reservation', [RegistrationController::class, 'exibitRegestration']);
    Route::get('show-registration-details/{id}', [RegistrationController::class, 'showRegistrationDetails']);

    Route::get('youthActivities-regestration', [RegistrationController::class, 'youthActivitiesRegestration']);
    Route::post('youthActivities-regestration', [RegistrationController::class, 'storeYouthActivitiesRegestration']);
});

Route::post('exhibits-reservation', [RegistrationController::class, 'stoteExibitRegestrationForm']);
Route::post('update_banner_image', [RegistrationController::class, 'updateBannerImage']);


Route::post('bookticketstore', [RegistrationController::class, 'RegesterUserStore']);

Route::get('registration-page-content-update', [RegistrationController::class, 'RegistrationPageContentedit'])->name('registration-page-content-update');
Route::patch('registration-page-content-update', [RegistrationController::class, 'RegistrationPageContentUpdate'])->name('registration-page-content-update');





Route::get('message_content/{id}', [HomeController::class, 'messageContent']);
Route::get('programs/{id}', [HomeController::class, 'programDetailsPage']);
Route::get('event-schedule/{id}', [HomeController::class, 'eventSchedule']);
Route::get('show-all-videos', [HomeController::class, 'showAllVideos']);
Route::get('more-donor-details/{id}', [HomeController::class, 'moreDonerDetails']);
Route::get('more-leadership-details/{id}', [HomeController::class, 'moreLeadershipDetails']);
Route::get('more-invitee-details/{id}', [HomeController::class, 'moreInviteeDetails']);
Route::get('more-committe-members/{id}', [HomeController::class, 'moreCommitteMembers']);
Route::get('show-events-on-calander', [HomeController::class, 'showEventsOnCalander']);


Route::get('api/get-schedule-on-date/{id}', [ScheduleController::class, 'getScheduleDatails']);
Route::get('api/get-donor-with-typeid/{id}', [HomeController::class, 'getDonorsWithTypeId']);
Route::get('api/get-leadership-with-typeid/{id}', [HomeController::class, 'getLeadershipsWithTypeId']);
Route::get('api/get-invitee-with-typeid/{id}', [HomeController::class, 'getinviteeWithTypeId']);
Route::get('api/get-invitee-with-typeid/{id}', [HomeController::class, 'getinviteeWithTypeId']);

Route::get('api/get-events-in-date', [HomeController::class, 'geteventsIndates']);
Route::get('api/get-details', [RegistrationController::class, 'getdetailsfromNriva']);

Route::get('{slug}', [HomeController::class, 'dinamicPage']);


















