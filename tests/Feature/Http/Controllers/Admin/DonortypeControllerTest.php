<?php

namespace Tests\Feature\Http\Controllers\Admin;

use App\Models\Admin\Donortype;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\Admin\DonortypeController
 */
class DonortypeControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $donortypes = Donortype::factory()->count(3)->create();

        $response = $this->get(route('donortype.index'));

        $response->assertOk();
        $response->assertViewIs('donortype.index');
        $response->assertViewHas('donortypes');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('donortype.create'));

        $response->assertOk();
        $response->assertViewIs('donortype.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\Admin\DonortypeController::class,
            'store',
            \App\Http\Requests\DonortypeStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $name = $this->faker->name;
        $status = $this->faker->boolean;

        $response = $this->post(route('donortype.store'), [
            'name' => $name,
            'status' => $status,
        ]);

        $donortypes = Donortype::query()
            ->where('name', $name)
            ->where('status', $status)
            ->get();
        $this->assertCount(1, $donortypes);
        $donortype = $donortypes->first();

        $response->assertRedirect(route('donortype.index'));
        $response->assertSessionHas('donortype.id', $donortype->id);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $donortype = Donortype::factory()->create();

        $response = $this->get(route('donortype.show', $donortype));

        $response->assertOk();
        $response->assertViewIs('donortype.show');
        $response->assertViewHas('donortype');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $donortype = Donortype::factory()->create();

        $response = $this->get(route('donortype.edit', $donortype));

        $response->assertOk();
        $response->assertViewIs('donortype.edit');
        $response->assertViewHas('donortype');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\Admin\DonortypeController::class,
            'update',
            \App\Http\Requests\DonortypeUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $donortype = Donortype::factory()->create();
        $name = $this->faker->name;
        $status = $this->faker->boolean;

        $response = $this->put(route('donortype.update', $donortype), [
            'name' => $name,
            'status' => $status,
        ]);

        $donortype->refresh();

        $response->assertRedirect(route('donortype.index'));
        $response->assertSessionHas('donortype.id', $donortype->id);

        $this->assertEquals($name, $donortype->name);
        $this->assertEquals($status, $donortype->status);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $donortype = Donortype::factory()->create();

        $response = $this->delete(route('donortype.destroy', $donortype));

        $response->assertRedirect(route('donortype.index'));

        $this->assertDeleted($donortype);
    }
}
