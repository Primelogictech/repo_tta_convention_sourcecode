<?php

namespace Tests\Feature\Http\Controllers\HomePage;

use App\Models\Admin\Donor;
use App\Models\Admin\Donortype;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\HomePage\DonorController
 */
class DonorControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $donors = Donor::factory()->count(3)->create();

        $response = $this->get(route('donor.index'));

        $response->assertOk();
        $response->assertViewIs('donor.index');
        $response->assertViewHas('donors');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('donor.create'));

        $response->assertOk();
        $response->assertViewIs('donor.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\DonorController::class,
            'store',
            \App\Http\Requests\DonorStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $donortype = Donortype::factory()->create();
        $donor_name = $this->faker->word;
        $description = $this->faker->text;
        $image_url = $this->faker->word;

        $response = $this->post(route('donor.store'), [
            'donortype_id' => $donortype->id,
            'donor_name' => $donor_name,
            'description' => $description,
            'image_url' => $image_url,
        ]);

        $donors = Donor::query()
            ->where('donortype_id', $donortype->id)
            ->where('donor_name', $donor_name)
            ->where('description', $description)
            ->where('image_url', $image_url)
            ->get();
        $this->assertCount(1, $donors);
        $donor = $donors->first();

        $response->assertRedirect(route('donor.index'));
        $response->assertSessionHas('donor.id', $donor->id);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $donor = Donor::factory()->create();

        $response = $this->get(route('donor.show', $donor));

        $response->assertOk();
        $response->assertViewIs('donor.show');
        $response->assertViewHas('donor');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $donor = Donor::factory()->create();

        $response = $this->get(route('donor.edit', $donor));

        $response->assertOk();
        $response->assertViewIs('donor.edit');
        $response->assertViewHas('donor');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\DonorController::class,
            'update',
            \App\Http\Requests\DonorUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $donor = Donor::factory()->create();
        $donortype = Donortype::factory()->create();
        $donor_name = $this->faker->word;
        $description = $this->faker->text;
        $image_url = $this->faker->word;

        $response = $this->put(route('donor.update', $donor), [
            'donortype_id' => $donortype->id,
            'donor_name' => $donor_name,
            'description' => $description,
            'image_url' => $image_url,
        ]);

        $donor->refresh();

        $response->assertRedirect(route('donor.index'));
        $response->assertSessionHas('donor.id', $donor->id);

        $this->assertEquals($donortype->id, $donor->donortype_id);
        $this->assertEquals($donor_name, $donor->donor_name);
        $this->assertEquals($description, $donor->description);
        $this->assertEquals($image_url, $donor->image_url);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $donor = Donor::factory()->create();

        $response = $this->delete(route('donor.destroy', $donor));

        $response->assertRedirect(route('donor.index'));

        $this->assertDeleted($donor);
    }
}
