<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerImages extends Model
{
    use HasFactory;


        protected $table = "banner_images";

     protected $fillable = [
         'user_id',
         'benfit_id',
         'image_url',
    ];
    protected $casts = [
        'count' => 'integer',
    ]; 

}
