<?php

function test()
{
    return "helper";
}


function UploadFile($path,$image, $file_name)
{
    $image->storeAs($path, $file_name);
}



function paymentDate($Payment_details)
{
    if($Payment_details->paymentmethord->name == config('conventions.paypal_name_db')){
        return $Payment_details->created_at;
    }

    if($Payment_details->paymentmethord->name == config('conventions.other_name_db')){
        return $Payment_details->more_info['transaction_date']?? "";
    }

    if($Payment_details->paymentmethord->name == config('conventions.check_name_db')){
        return $Payment_details->more_info['cheque_date']?? "";
    }

    if($Payment_details->paymentmethord->name == config('conventions.zelle_name_db')){
        return $Payment_details->created_at;
    }


}