<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Admin\Event;

class ScheduleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $event = Event::find($this->event_id);
        //dd($event);
        return [
            'event_id' => ['required'],
            //'date' => ['required', 'date'],
            'from_time.*' => ['required'],
            'to_time.*' => ['required'],
            'program_name.*' => ['required'],
            'room_no.*' => ['required'],
            'date' => [
                'required',
                'after_or_equal:' . ($event->from_date)->format('Y-m-d'),
                'before_or_equal:' . ($event->to_date)->format('Y-m-d')
            ],
        ];
    }
}
