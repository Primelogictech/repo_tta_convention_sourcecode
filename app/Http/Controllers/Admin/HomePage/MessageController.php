<?php

namespace App\Http\Controllers\Admin\HomePage;

use App\Http\Controllers\Controller;
use App\Models\Admin\Message;
use App\Models\Admin\Designation;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = Message::with('designation')->get();
        return view('admin.homePageContentUpdate.message.viewmessage', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $disignations = Designation::active()->get();
        return view('admin.homePageContentUpdate.message.addmessage' , compact('disignations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'designation_id' => ['required'],
            'name' => ['required',],
            'message' => ['required',],
            'image' => [
                'required', 'image', 'mimes:jpg,bmp,png', 'max:1024',
                'dimensions:ratio=1/1'
            ],
        ];
        $request->validate($rules);


        $message = Message::create($request->all());
        if ($request->hasFile('image')) {
            $file_name = 'Message' . '_' . $message->id . '.' . $request->image->getClientOriginalExtension();
            $request->file('image')->storeAs(config('conventions.message_upload'), $file_name);
        }else {
             $file_name=null;
        }

        $message->image_url = $file_name;
        $message->save();

        return redirect()
        ->route('message.index')
        ->with('message-suc', 'Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        $disignations = Designation::active()->get();
        return view('admin.homePageContentUpdate.message.editmessage', compact('message', 'disignations'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        $rules = [
            'designation_id' => ['required'],
            'name' => ['required',],
            'message' => ['required',],
            'image' => [
                 'image', 'mimes:jpg,bmp,png', 'max:1024',
                'dimensions:ratio=1/1'
            ],
        ];
        $request->validate($rules);

        if($request->hasFile('image')){
            $file_name = 'Message' . '_' . $message->id . '.' . $request->image->getClientOriginalExtension();
            $request->file('image')->storeAs(config('conventions.message_upload'), $file_name);

            $message->image_url = $file_name;
            $message->save();
        }

         Message::where("id",$message->id)
        ->update([
            'message' => $request->message,
            'designation_id' => $request->designation_id,
            'name' => $request->name,
        ]);

        return redirect()
            ->route('message.index')
            ->with('message-suc', 'updated');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        $message = Message::find($message->id);
        if (file_exists(config('conventions.message_display') . $message->image_url)) {
            unlink(config('conventions.message_display') . $message->image_url);
        }
       return $message->delete();
    }



    public function updateStatus(Request $request)
    {
        return Message::where('id', $request->id)->update(['status' => $request->status]);
    }


}
