<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Registration;
use Illuminate\Http\Request;

class AdminRegistrationController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function exhibitRegistrations()
    {
        $registrations=Registration::where('registration_type_name','Exhibit')->get();
        return view('admin.registrations.exhibitRegistrations', compact('registrations') );
    }



     public function youthActivitiesRegistrations()
    {
        $registrations=Registration::where('registration_type_name','YouthActivities')->get();
        return view('admin.registrations.exhibitRegistrations', compact('registrations') );
    }


}
