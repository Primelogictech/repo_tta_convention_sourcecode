<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommitteStoreRequest;
use App\Http\Requests\CommitteUpdateRequest;
use App\Models\Admin\Committe;
use App\Models\Admin\Event;
use App\Models\Admin\Program;
use App\Models\Admin\RolesAndDesignations;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reg_users =User::where('registration_type_id', '<>', null)->count();

        $committe_count =RolesAndDesignations::where('type','committe')->count();
        $donorsType_count =RolesAndDesignations::where('type', 'donorsType')->count();
        $inviteestype_count =RolesAndDesignations::where('type', 'inviteestype')->count();
        $leadershiptype_count =RolesAndDesignations::where('type', 'leadershiptype')->count();
        $program_count =Program::where('status', 1)->count();
        $event_count =Event::where('status', 1)->count();
        $donotion_amount=User::where('registration_type_id', '<>', null)->sum('amount_paid');

        $count =[
            'reg_users'=> $reg_users,
            "committe_count" =>  $committe_count,
            "donorsType_count"=> $donorsType_count,
            'inviteestype_count' => $inviteestype_count,
            'leadershiptype_count'=> $leadershiptype_count,
            'donotion_amount'=> $donotion_amount,
            'program_count' => $program_count,
            'event_count' => $event_count,
        ];
        return view('admin.dashboard',compact('count'));
    }


}
