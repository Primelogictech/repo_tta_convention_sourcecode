<?php

namespace App\Http\Controllers;

use App\Models\Admin\SponsorCategory;
use App\Models\Admin\SponsorCategoryType;
use App\Models\Admin\RegistrationContent;
use App\Http\Controllers\Admin\PaymentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use App\Models\Admin\ExhibitorType;

use App\Http\Controllers\Controller;
use App\Models\Admin\Paymenttype;
use App\Models\Admin\BannerImages;
use App\Models\User;
use App\Models\Registration;
use App\Models\Payment as ModelPayment;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use App\Models\Admin\Venue;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Carbon\Carbon;
use URL;
use Session;
use Config;
use DB;

class RegistrationController extends Controller
{

    public function __construct()
    {
        $this->_api_context = "";
    }


    public function ShowRegistrationForm()
    {
        session::forget('payment_id');
        session::forget('sponsor_category');
        session::forget('category_id');
        session::forget('Individual_amount');
        $user=User::find(Auth::user()->id);
        $SponsorCategoryTypes= SponsorCategoryType::where('status',1)->get();
        $Individuals= SponsorCategory::where('status',1)->where('donor_type_id', 0)->get();
        $donors= SponsorCategory::with('donortype')->where('status',1)->where('donor_type_id','!=' ,0)->get();
        $RegistrationContent = RegistrationContent::first();
        $paymenttype = Paymenttype::where('status',1)->get();
        $states=DB::table('states')->get();
       // dd($donors[0]->benfits[0]->name);
        return view('registration',compact("SponsorCategoryTypes", 'Individuals', 'donors', 'RegistrationContent', 'user', 'paymenttype', 'states'));
    }
//
    public function RegesterUserStore(Request $request)
    {   
        if ($request->hasfile('benfitimage')) {
                $rules= [
                    'benfitimage.*' => ['image','mimes:jpg,png','max:5120',
                ],
              ];
              $request->validate($rules);
          }

        if($request->category=="Donor"){
            $SponsorCategory=SponsorCategory::find($request->sponsor_category);
            $benfits_Ids= $SponsorCategory->benfits->pluck('id')->toArray();
        }

        $user = user::where('email',$request->email)->first();
        if($request->is_upgrade== 'upgrade'){
            $is_upgrade = true;
            $request->category= 'Donor';
        }else {
            $is_upgrade = false;
        }
        if($request->has('pay_partial_amount')){
            $paying_amount=$request->paying_amount;
        }else{
            if($request->category == 'Donor'){
                $paying_amount= $request->donation_amount_hidden;
                if($is_upgrade){
                    $paying_amount = $paying_amount- $user->amount_paid;
                }
            }else{
                $paying_amount= $request->registration_amount_hidden;
            }
        }
        $Paymenttype = Paymenttype::find($request->payment_type);

        $user = User::where('email', $request->email)->first();

        $user->spouse_full_name = $request->spouse_full_name;
        $user->children_count = [
                            'below_6'=>$request->below_6,
                            'age_7_to_15'=>$request->age_7_to_15,
                            'age_16_to_23'=>$request->age_16_to_23
                        ];
        $user->save();

        if (!isset($user)) {
            $user = User::create($request->all());
            if ($request->hasfile('image')) {
                $file_name = 'user' . '_' . $user->id . '.' . $request->image->getClientOriginalExtension();
                $request->file('image')->storeAs(config('conventions.user_upload'), $file_name);
                user::where('id', $user->id)->update(['image_url' => $file_name]);
            }
        }

        if($request->category == 'Donor'){
            $category=SponsorCategoryType::where('name', 'Donor')->first();
            $SponsorCategory  = SponsorCategory::find($request->sponsor_category);
            $user = User::where('email', $request->email)->first();

             if ($request->hasfile('benfitimage')) {
              foreach ($request->benfitimage as $key => $image) {
                  $key=explode("_",$key);
                    if($key[0]==$request->sponsor_category){
                        $key=$key[1];
                            $file_name = 'benfit_id' . '_' .$key."_". $user->id . '.' . $image->getClientOriginalExtension();
                            $image->storeAs(config('conventions.benfit_image_upload'), $file_name);
                                $temp = [
                                    'user_id' => $user->id,
                                    'benfit_id' => $key,
                                    'image_url' => $file_name,
                                ];
                            $BannerImages = BannerImages::create($temp);
                    }
                }
            }

            // call payment methord
            if($Paymenttype->name== Config('conventions.paypal_name_db')){
                $user= Auth::user();
                $user->total_amount = $request->donation_amount_hidden;
                $user->save();
                return  $this->PaypalPayment($paying_amount, $user->id, $request, $Paymenttype->id, $is_upgrade);
            }else{
                $this->Dopayment($request,  $user,  $paying_amount,$is_upgrade);

                $user->sponsorship_category_id = $SponsorCategory->id;
                if($request->has('pay_partial_amount')){
                    $user->payment_status = 'Partial Paid';
                }else{
                    $user->payment_status = 'Paid';
                }
                $user->total_amount = $request->donation_amount_hidden;
                if($is_upgrade){
                    $user->amount_paid = $user->amount_paid+$paying_amount;
                }else{
                    $user->amount_paid = $paying_amount;
                }
                $user->registration_type_id = $category->id;
                $user->save();
                return redirect('myaccount');
            }
        }

        if($request->category== "Family / Individual")
        {
            $category = SponsorCategoryType::where('name', 'Family / Individual')->first();
            $SponsorCategory  = SponsorCategory::find($request->sponsor_category);
            $individual_registration=[];
            $total_amount = 0;
            foreach ($request->count as $key => $value) {
                $Individual = SponsorCategory::where('status', 1)->where('id',$key)->first();
                $a=[
                    $key => $value
                ];
                $individual_registration = $a+ $individual_registration;
            }

            $total_amount = $request->registration_amount_hidden;
            $user = User::where('email', $request->email)->first();
            // call payment methord

            if ($Paymenttype->name == Config('conventions.paypal_name_db')) {
                Session::put('individual_registration', $individual_registration);
                Session::put('Individual_amount', $total_amount);
                return  $this->PaypalPayment($paying_amount, $user->id, $request, $Paymenttype->id);
            } else {
                $this->Dopayment($request,  $user,  $paying_amount);

                $user->individual_registration =  $individual_registration;
                if($request->has('pay_partial_amount')){
                    $user->payment_status = 'Partial Paid';
                }else{
                    $user->payment_status = 'Paid';
                }
                $user->total_amount = $total_amount;
                $user->amount_paid = $paying_amount;
                $user->registration_type_id = $category->id;
                $user->save();
                //myaccount.blade
                return redirect('myaccount'); //;
            }
        }
      // return  $this->ShowRegistrationForm();
       // return redirect(RouteServiceProvider::HOME);
    }

    public function PaypalPayment($amount, $user_id, $request,$payment_id, $is_upgrade=false)
    {

        $settings = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(env('PAYPAL_SANDBOX_CLIENT_ID'), env('PAYPAL_SANDBOX_CLIENT_SECRET')));
        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings);
        $payer = new Payer();

        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();
        $userCurrency = 'USD';
        $pay_amount = $amount;
        $item_1->setName('NRIVA Convention test')
        ->setCurrency($userCurrency)
            ->setQuantity(1)
            ->setPrice($pay_amount);


        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency($userCurrency)
            ->setTotal($pay_amount);
        $timestamp = Carbon::now()->toDateTimeString();

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setInvoiceNumber($timestamp . '_' . '$donation->id')
            ->setItemList($item_list)
            ->setDescription('Transaction Details');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('payment.status'))
        ->setCancelUrl(URL::route('payment.status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
        ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));


        try {
            $payment->create($this->_api_context);
            if($is_upgrade){
                $account_status= 'Plan Upgrade';
            }else{
                $account_status= 'Initial Payment';
            }
            $temp = [
                'user_id' => $user_id,
                'payment_amount' => $pay_amount,
                'payment_methord' => $payment_id,
                'unique_id_for_payment' => $payment->id,
                'more_info' => $request->more_info,
                'payment_status' => 'pending',
                'account_status' => $account_status,
                'payment_made_towards' => "Convention Registration"
            ];
            $Modelpayment = ModelPayment::create($temp);

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {

            if (\Config::get('app.debug')) {
                return redirect()->back()->withError('Connection timeout');
            } else {
                return redirect()->back()->withError('Some error occur, sorry for inconvenient');
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        Session::put('payment_id', $Modelpayment->id);
        Session::put('sponsor_category', $request->sponsor_category);
        $SponsorCategoryType=SponsorCategoryType::where('name', $request->category)->first();
        Session::put('category_id', $SponsorCategoryType->id);

        if (isset($redirect_url)) {
            return redirect($redirect_url);
        }

    }

    public function Dopayment($request, $user,  $paying_amount,$is_upgrade=false)
    {

        if ($is_upgrade) {
            $account_status = 'Plan Upgrade';
        } else {
            $account_status = 'Initial Payment';
        }
        $Paymenttype=Paymenttype::find($request->payment_type);

        if($Paymenttype->name== Config('conventions.check_name_db')){
            $temp=[
                'user_id'=> $user->id,
                'payment_amount'=> $paying_amount,
                'payment_methord'=> $Paymenttype->id,
                'unique_id_for_payment'=>$request->cheque_number,
                'more_info'=> $request->more_info,
                'payment_status' => 'Pending',
                'account_status' => $account_status,
                'payment_made_towards' => "Convention Registration"
            ];
            $payment = ModelPayment::create($temp);

        }
        if($Paymenttype->name== Config('conventions.zelle_name_db')){
            $more_info = [
                'cheque_date' => $request->cheque_date,
                'more_info' =>  $request->more_info
            ];
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->Zelle_Reference_Number,
                'more_info' =>   $more_info,
                'payment_status' => 'Inprocess',
                'account_status' => $account_status,
                'payment_made_towards' => "Convention Registration"
            ];
            $payment = ModelPayment::create($temp);
        }

        if ($Paymenttype->name == Config('conventions.other_name_db')) {
            $more_info=[
                'transaction_date'=> $request->transaction_date,
                'Payment_made_through'=> $request->Payment_made_through,
            ];
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->transaction_id,
                'more_info' => $more_info,
                'payment_status' => 'Inprocess',
                'account_status' => $account_status,
                'payment_made_towards' => "Convention Registration"
            ];
            $payment = ModelPayment::create($temp);
        }



    }

    public function RegistrationPageContentedit()
    {
        $RegistrationContent=RegistrationContent::first();
        $paymenttypes = Paymenttype::where('status', 1)->get();
        return view('admin.ContentMangement.Registration.contentupdate', compact('RegistrationContent', 'paymenttypes'));
    }

    public function RegistrationPageContentUpdate(Request $request)
    {
        $RegistrationContent = RegistrationContent::first();
        $RegistrationContent->update($request->all());
        foreach ($request->payment_note as $id => $note) {
            Paymenttype::where('id', $id)->update(['note' => $note]);
        }
        $paymenttypes = Paymenttype::where('status', 1)->get();
          return redirect('registration-page-content-update');
      //  return view('admin.ContentMangement.Registration.contentupdate', compact('RegistrationContent', 'paymenttypes'));
    }

    public function getdetailsfromNriva(Request $request)
    {
     //   $response = Http::get('http://localhost/nriva/public/api/get-details?email='.$request->email);
        $response = Http::get(env('NRIVA_URL').'/api/get-details?email='.$request->email);
        return $response;
    }


    public function showRegistrations()
    {
        $users=User::with('registrationtype')->where('email','!=',env('admin_email'))->get();
        return view('admin.registrations.index', compact('users'));

    }

    public function showAssigneFeatures($id)
    {
        $user=User::FindOrFail($id);
        $user_benfits= $user->benfits;
        $bannerimages= $user->bannerimages;
        return view('admin.registrations.assigneFeatures', compact('user','user_benfits','bannerimages'));
    }

    public function updateAssigneFeatures(Request $request)
    {
        $user=User::find($request->user_id);
        $ben=$user->benfits()->where('benfit_id', $request->id)->first();
        if(isset($ben)){
            return   $user->benfits()->updateExistingPivot($request->id,['content' => $request->content]);
        }else{
            return   $user->benfits()->attach($request->id,['content' => $request->content]);
        }
    }

    public function myaccount()
    {
        $id=auth::user()->id;
        $registrations= Registration::where('user_id',$id)->get();

        return view('myaccount', compact('registrations'));
    }


    public function PaymentHistory()
    {
            $payments=ModelPayment::where('user_id', Auth::user()->id)->with('paymentmethord')->get();
            return view('PaymentHistory',compact('payments'));
    }

    public function adminSidePaymentHistory($user_id)
    {
        $payments = ModelPayment::where('user_id', $user_id)->with('paymentmethord')->get();
        return view('admin.registrations.adminSidePymentHistory', compact('payments'));
    }

    public function printTicket()
    {
        $venue = Venue::first();

 //       $payments = ModelPayment::where('user_id', Auth::user()->id)->with('paymentmethord')->get();
        return view('printticket', compact('venue'));
    }

    public function viewfeatures()
    {
        $user= Auth::user();
        $categorydetails= $user->categorydetails;
        $benfits=$user->benfits;
        $bannerimages=$user->bannerimages;
        return view('viewfeatures', compact('categorydetails', 'benfits','bannerimages'));
    }

    public function showAddNoteForm($id)
    {
        $user =User::findOrFail($id);
        return view('admin.registrations.addNote', compact('user'));
    }

    public function storeAddNoteForm(Request $request)
    {
        //registration_note
        User::where('id', $request->user_id)->update([
            'registration_note'=>$request->registration_note
        ]);
        return $this->showAddNoteForm($request->user_id);
    }


    public function exibitRegestration()
    {
        $exhibitsTypes= ExhibitorType::where('status',1)->get();
        $RegistrationContent = RegistrationContent::first();
        $paymenttypes = Paymenttype::where('status', 1)->get();

        $Venue = Venue::first();

        return view('registration.showexhibitsRegistrationForm', compact('exhibitsTypes', 'RegistrationContent', 'paymenttypes', 'Venue'));
    }

    public function youthActivitiesRegestration()
    {
        return view('registration.youthActivitiesRegistration');
    }

    public function storeYouthActivitiesRegestration(Request $request)
    {
           $data=array_merge($request->all(),
        [
            'user_id' => Auth::user()->id,
            'registration_type_name' => "YouthActivities"
            ]
        );
          Registration::create($data);
        return view('registration.youthActivitiesRegistration')->with('message-suc', "Youth Activities registered successfully");;
    }

    public function stoteExibitRegestrationForm(Request $request )
    {
        $data=array_merge($request->all(),
        [
            'user_id' => Auth::user()->id,
            'registration_type_name' => "Exhibit"
            ]
        );
          Registration::create($data);
        $payment_made_towards = "Paid Towards Exhibit registration";
        $paymenttype= Paymenttype::find($request->payment_type);

        if($paymenttype->name== Config('conventions.check_name_db')){
            App(PaymentController::Class)->PayWithCheck(Auth::user()->id, $request->amount, $paymenttype->id, $payment_made_towards, $request );
        }

        if ($paymenttype->name == Config('conventions.zelle_name_db')) {
            App(PaymentController::Class)->PayWithZelle(Auth::user()->id, $request->amount, $paymenttype->id, $payment_made_towards, $request);
        }

        if ($paymenttype->name == Config('conventions.other_name_db')) {
            App(PaymentController::Class)->PayWithOther(Auth::user()->id, $request->amount, $paymenttype->id, $payment_made_towards, $request);
        }

        if ($paymenttype->name == Config('conventions.paypal_name_db')) {
                $temp = [
                    'user_id' => Auth::user()->id,
                    'payment_amount' => $request->amount,
                    'payment_methord' => $paymenttype->id,
                    'payment_status' => 'Pending'
                ];
                $payment = ModelPayment::create($temp);

            return  App(PaymentController::Class)
                ->PayWithPayPal($request->amount, 'Exhibit Regestration-'.$request->full_name."-". Auth::user()->id,  $payment->id, 'paypal.status');
        }
        return redirect('myaccount')->with('message-suc', "Exhibit registered successfully");
    }

    public function showRegistrationDetails($id)
    {
        $Registration=Registration::where('id',$id)->where('user_id', auth::user()->id)->firstOrFail();
        return view('registration.showRegistrationDetails',compact('Registration'));
    }


    public function download($id) {
        $BannerImages = BannerImages::findorFail($id);
        return \Storage::download(  config('conventions.benfit_image_upload') . '/'. $BannerImages->image_url);
    }


    public function updateBannerImage(Request $request)
    {
        if ($request->hasfile('benfitimage')) {
            $key=$request->benfit_id;
            $image=$request->benfitimage[$key];
            $user_id =Auth::user()->id;

             $BannerImage=BannerImages::where('user_id', $user_id)
                ->where('benfit_id',$request->benfit_id)->first();
               if($BannerImage){
                if(file_exists(config('conventions.benfit_image_display').$BannerImage->image_url)){
                    unlink(config('conventions.benfit_image_display').$BannerImage->image_url);  
                }
               }

                $file_name = 'benfit_id' . '_' .$key."_". $user_id . '.' . $image->getClientOriginalExtension();
                $image->storeAs(config('conventions.benfit_image_upload'), $file_name);
                   
                
                if(!$BannerImage){
                      $temp = [
                        'user_id' => $user_id,
                        'benfit_id' => $key,
                        'image_url' => $file_name,
                    ];
                    $BannerImages = BannerImages::create($temp);
                }else{
                    $BannerImage->image_url=$file_name;
                    $BannerImage->save();
                }
            }
         return redirect('viewfeatuees');
    }

}
