<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->float('payment_amount', 8, 2);
            $table->string('payment_made_towards');
            $table->boolean('status')->comment("successful payment->1 payment faild->0")->default(0);
            $table->string('payment_methord');
            $table->string('unique_id_for_payment')->comment('rererence id for zelle, payment_id for paypal')->nullable();
            $table->string('more_info')->nullable();
            $table->string('account_status');
            $table->string('payment_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
