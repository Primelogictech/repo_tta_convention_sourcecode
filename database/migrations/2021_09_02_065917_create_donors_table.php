<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('donors', function (Blueprint $table) {
            $table->id();
            $table->foreignId('donortype_id');
            $table->string('donor_name', 100);
            $table->longText('description');
            $table->string('image_url', 100);
            $table->boolean('status')->default(1)->comment("active->1 inactive->0");
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donors');
    }
}
