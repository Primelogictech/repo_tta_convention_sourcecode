<?php

namespace Database\Factories\Admin;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Admin\Program;

class ProgramFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Program::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'chair_name' => $this->faker->regexify('[A-Za-z0-9]{100}'),
            'mobile_number' => $this->faker->regexify('[A-Za-z0-9]{20}'),
            'co_chair_name' => $this->faker->regexify('[A-Za-z0-9]{100}'),
            'program_name' => $this->faker->regexify('[A-Za-z0-9]{20}'),
            'Location' => $this->faker->regexify('[A-Za-z0-9]{200}'),
            'date' => $this->faker->date(),
            'page_content' => $this->faker->text,
            'status' => $this->faker->boolean,
        ];
    }
}
