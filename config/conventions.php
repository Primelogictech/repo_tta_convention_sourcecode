<?php
return [
    'banner_upload' =>'public/banner/',
    'banner_display' =>'public/storage/banner/',

    'message_upload' => 'public/message/',
    'message_display' => 'public/storage/message/',

    'logo_upload' => 'public/logo/',
    'logo_display' => 'public/storage/logo/',

    'event_upload' => 'public/event/',
    'event_display' => 'public/storage/event/',

    'program_upload' => 'public/program/',
    'program_display' => 'public/storage/program/',


    'donor_upload' => 'public/donor/',
    'donor_display' => 'public/storage/donor/',

    'member_upload' => 'public/member/',
    'member_display' => 'public/storage/member/',

    'user_upload' => 'public/user/',
    'user_display' => 'public/storage/user/',


    'user_upload' => 'public/user/',
    'user_display' => 'public/storage/user/',


    'committe_upload' => 'public/committe/',
    'committe_display' => 'public/storage/committe/',

    'benfit_image_upload' => 'public/benfit_image/',
    'benfit_image_display' => 'public/storage/benfit_image/',
    
    'paypal_name_db' => 'Paypal',
    'zelle_name_db' => 'Zelle',
    'check_name_db' => 'Check',
    'other_name_db' => 'Other',
];
